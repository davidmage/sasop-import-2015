Imports System.Globalization
Imports System.Threading
Public Class Form1
    Inherits System.Windows.Forms.Form

    'Deklarerar pekare
    Dim pdata As Long
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Dim blnSpcs As Boolean = False


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        Dim regKey As Microsoft.Win32.RegistryKey

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Recommit", True)
        If Not regKey Is Nothing Then
            txtProg.Text = regKey.GetValue("SASOP_program_path")
            txtFtg.Text = regKey.GetValue("SASOP_company_path")
            TextBox1.Text = regKey.GetValue("SASOP_file_path")
            logPath.Text = regKey.GetValue("SASOP_log_path")
            regKey.Close()
        End If
        If (txtProg.Text = "") Then
            txtProg.Text = "\\SwedenFS\SPCS_Administration\Gemensamma filer"
        End If
        If (txtFtg.Text = "") Then
            txtFtg.Text = "\\SwedenFS\SPCS_Administration\F�retag\FTG1"
        End If
        If (logPath.Text = "") Then
            logPath.Text = "\\SwedenFS\SPCS\SPCS\Importlog"
        End If


        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents txtProg As System.Windows.Forms.TextBox
    Friend WithEvents txtFtg As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Public WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents importstatus As System.Windows.Forms.ProgressBar
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents label6 As System.Windows.Forms.Label
    Public WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents quitButton As System.Windows.Forms.Button
    Public WithEvents logPath As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtProg = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFtg = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.importstatus = New System.Windows.Forms.ProgressBar()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.logPath = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.quitButton = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(40, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(184, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Ange s�kv�g till programkatalogen"
        '
        'txtProg
        '
        Me.txtProg.Location = New System.Drawing.Point(40, 72)
        Me.txtProg.Name = "txtProg"
        Me.txtProg.Size = New System.Drawing.Size(568, 20)
        Me.txtProg.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(40, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Ange s�kv�g till f�retagskatalogen"
        '
        'txtFtg
        '
        Me.txtFtg.Location = New System.Drawing.Point(16, 104)
        Me.txtFtg.Name = "txtFtg"
        Me.txtFtg.Size = New System.Drawing.Size(568, 20)
        Me.txtFtg.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(432, 136)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(152, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Anslut till SPCS"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(808, 776)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Avsluta"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(160, 32)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(344, 20)
        Me.TextBox1.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(24, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 23)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Loggfils mapp"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(8, 432)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox2.Size = New System.Drawing.Size(910, 96)
        Me.TextBox2.TabIndex = 11
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.PictureBox1)
        Me.GroupBox2.Location = New System.Drawing.Point(640, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(270, 96)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Utvecklat av"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(15, 23)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 62)
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Multiselect = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(520, 32)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(56, 24)
        Me.Button7.TabIndex = 14
        Me.Button7.Text = "V�lj fil"
        '
        'Button8
        '
        Me.Button8.Enabled = False
        Me.Button8.Location = New System.Drawing.Point(584, 32)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(104, 23)
        Me.Button8.TabIndex = 15
        Me.Button8.Text = "Starta filimport"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.txtFtg)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(600, 168)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Programinst�llningar till Visma/SPCS"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(16, 136)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(24, 24)
        Me.Panel1.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(48, 144)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 16)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Fr�nkopplad"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 416)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Statuslogg"
        '
        'importstatus
        '
        Me.importstatus.Location = New System.Drawing.Point(32, 320)
        Me.importstatus.Name = "importstatus"
        Me.importstatus.Size = New System.Drawing.Size(752, 24)
        Me.importstatus.Step = 1
        Me.importstatus.TabIndex = 20
        Me.importstatus.Visible = False
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(32, 296)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 23)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Importstatus"
        Me.Label4.Visible = False
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(32, 360)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 23)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Bearbetar rad:"
        Me.Label5.Visible = False
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(88, 152)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(120, 23)
        Me.label6.TabIndex = 23
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.logPath)
        Me.GroupBox4.Controls.Add(Me.label6)
        Me.GroupBox4.Controls.Add(Me.Button8)
        Me.GroupBox4.Controls.Add(Me.Button7)
        Me.GroupBox4.Controls.Add(Me.TextBox1)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(902, 192)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Import"
        '
        'logPath
        '
        Me.logPath.Location = New System.Drawing.Point(160, 64)
        Me.logPath.Name = "logPath"
        Me.logPath.Size = New System.Drawing.Size(344, 20)
        Me.logPath.TabIndex = 24
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(24, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 23)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Fil att importera"
        '
        'quitButton
        '
        Me.quitButton.Location = New System.Drawing.Point(766, 538)
        Me.quitButton.Name = "quitButton"
        Me.quitButton.Size = New System.Drawing.Size(152, 24)
        Me.quitButton.TabIndex = 25
        Me.quitButton.Text = "Avsluta"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Location = New System.Drawing.Point(640, 128)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(270, 64)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Version"
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(16, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(176, 24)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "SASOP Import v1.14"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(929, 574)
        Me.Controls.Add(Me.quitButton)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.importstatus)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtProg)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "SASOP / SPCS Import Starkey Sweden AB "
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim Err As ADKERROR
        If blnSpcs Then
            blnSpcs = False
            Panel1.BackColor = System.Drawing.Color.Red
            If pdata <> 0 Then

                Err = AdkDeleteStruct(pdata)
                If Err.lRc > 0 Then
                    Call GetErrorText(Err)
                End If
            End If
            Call AdkClose()
            Button1.Text = "Anslut till SPCS"
            Label8.Text = "Fr�nkopplad"

        Else
            Label8.Text = "Ansluter..."
            Label8.Refresh()
            Panel1.BackColor = System.Drawing.Color.Yellow
            Panel1.Refresh()
            Dim regKey As Microsoft.Win32.RegistryKey

            If txtFtg.Text = "" Or txtProg.Text = "" Then
                MsgBox("Du m�ste fylla i s�kv�garna!")
                Exit Sub
            End If


            regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Recommit", True)
            If regKey Is Nothing Then
                regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE", True)
                regKey.CreateSubKey("Recommit")
                regKey.Close()
            End If

            regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Recommit", True)
            regKey.SetValue("SASOP_company_path", txtFtg.Text)
            regKey.SetValue("SASOP_program_path", txtProg.Text)
            regKey.Close()




            Err = AdkOpen(txtProg.Text, txtFtg.Text)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                MsgBox("Uppkopplingen misslyckades!")
                Label8.Text = "Fr�nkopplad"
                Panel1.BackColor = System.Drawing.Color.Red
                Exit Sub
            End If
            blnSpcs = True
            Panel1.BackColor = System.Drawing.Color.Green
            Label8.Text = "Ansluten"
            Button8.Enabled = True
            Button1.Text = "Koppla ifr�n SPCS"

        End If
    End Sub








    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim oFile As System.IO.File
        Dim oRead As System.IO.StreamReader
        Dim strLineIn As String
        Dim arrData() As String
        Dim intCounter As Int16
        Dim strLogtext As String
        oRead = oFile.OpenText(TextBox1.Text)
        intCounter = 0
        strLogtext = "---------------------- New import  " & TextBox1.Text & " ---------------------------------"
        TextBox2.Text = strLogtext & vbCrLf
        addtolog("c:\spcs\logfile.txt", strLogtext)
        While oRead.Peek <> -1
            intCounter = intCounter + 1
            strLineIn = oRead.ReadLine()
            strLogtext = Now() & "|" & TextBox1.Text & "| Row " & intCounter
            addtolog("c:\spcs\logfile.txt", strLogtext)
            TextBox2.Text = TextBox2.Text & strLogtext & vbCrLf
            arrData = strLineIn.Split("|")
            '            For intIndex = 0 To arrData.GetUpperBound(0)
            '           MsgBox(arrData(intIndex))
            '          Next
            'addcustomer(arrData(2) & arrData(3), arrData(4), arrData(5), arrData(6))
        End While

        oRead.Close()
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("sv-SE")
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            With OpenFileDialog1
                'With statement is used to execute statements using a particular object, here,_
                'OpenFileDialog1
                .Filter = "Text files (*.txt)|*.txt|" & "All files|*.*"
                'setting filters so that Text files and All Files choice appears in the Files of Type box
                'in the dialog
                If .ShowDialog() = DialogResult.OK Then
                    Dim intIndex As Integer
                    'showDialog method makes the dialog box visible at run time
                    For intIndex = 0 To .FileNames.GetUpperBound(0)
                        TextBox1.Text = .FileNames.GetValue(intIndex)
                    Next
                End If
            End With
        Catch es As Exception
            MessageBox.Show(es.Message)
        Finally

        End Try
    End Sub

    Public Sub updatearticles(ByVal strUrl As String, ByVal pstrUID As String, ByVal pstrPWD As String)
        Dim objWc As New System.Net.WebClient
        Dim ncCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(pstrUID, pstrPWD)
        Dim arrArticledata() As String
        Dim intNrofarticles As Integer
        Dim intcounter As Integer
        TextBox2.Text = TextBox2.Text & vbCrLf & "-------------------- start import av artiklar ---------------------" & vbCrLf & vbCrLf
        objWc.Credentials = ncCred
        TextBox2.Text = TextBox2.Text & "Ansluter till server... " & vbCrLf
        On Error Resume Next
        Dim strHtml As String = System.Text.Encoding.UTF7.GetString(objWc.DownloadData(strUrl))
        If Err.Number <> 0 Then
            TextBox2.Text = TextBox2.Text & "Anslutningen misslyckades " & vbCrLf
            TextBox2.Text = TextBox2.Text & Err.Description & vbCrLf
            TextBox2.Text = TextBox2.Text & vbCrLf & "-------------------- slut import av artiklar --------------------- " & vbCrLf
            Exit Sub
        End If
        On Error GoTo -1
        arrArticledata = strHtml.Split(";")
        intNrofarticles = UBound(arrArticledata, 1) / 3
        For intcounter = 0 To intNrofarticles - 1
            TextBox2.Update()
            addarticle(arrArticledata(intcounter * 3), arrArticledata(intcounter * 3 + 1), arrArticledata(intcounter * 3 + 2))

            TextBox2.Text = TextBox2.Text & intcounter & vbTab & arrArticledata(intcounter * 3) & vbTab & arrArticledata(intcounter * 3 + 1) & vbTab & arrArticledata(intcounter * 3 + 2) & vbCrLf

        Next
        TextBox2.Text = TextBox2.Text & vbCrLf & "-------------------- slut import av artiklar --------------------- " & vbCrLf
        If functions.errFlag = True Then
            MsgBox("Artikelimporten misslyckades!", MsgBoxStyle.Critical)
        Else
            MsgBox("Artikelimporten lyckades!", MsgBoxStyle.Information)
        End If
    End Sub





    Private Sub importorders()
        Dim oRead As System.IO.StreamReader
        Dim strLineIn As String
        Dim intNrofrows As Integer
        Dim arrData() As String
        Dim intCounter As Int16
        Dim strLogtext As String
        Dim strKundNr As String
        Dim strCompany As String
        Dim strAddress1 As String
        Dim strAddress2 As String
        Dim strPostalcode As String
        Dim strCity As String
        Dim strCountry As String
        Dim strCountryCode As String
        Dim dteOrderdate As Date
        Dim intOldorderid As Integer
        Dim intOrderid As Integer
        Dim blnFirst As Boolean = True
        Dim arrArticle()() As String
        Dim blnIntercompany As Boolean
        Dim strPaymentterms As String
        Dim strInvoicetype As String
        Dim strCurrency As String
        Dim intNroflines As Integer
        Dim strArticleName As String
        Dim dblArticlePrice As Double
        Dim strArticleAccount As String
        Dim strVatCode As String
        Dim strMomskod As String
        Dim intActiveRow As Integer


        '       Lagra undan s�kv�gen i registryt
        Dim regKey As Microsoft.Win32.RegistryKey

        '        If txtFtg.Text = "" Or txtProg.Text = "" Then
        '       MsgBox("Du m�ste fylla i s�kv�garna!")
        '      Exit Sub
        '     End If


        regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Recommit", True)
        If regKey Is Nothing Then
            regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE", True)
            regKey.CreateSubKey("Recommit")
            regKey.Close()
        End If
        intNroflines = CountLinesFromFile(TextBox1.Text)

        regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Recommit", True)
        regKey.SetValue("SASOP_file_path", TextBox1.Text)
        regKey.SetValue("SASOP_log_path", logPath.Text)
        regKey.Close()


        'oRead = oFile.OpenText(TextBox1.Text)
        oRead = New IO.StreamReader(TextBox1.Text, System.Text.Encoding.UTF7)
        intCounter = 0
        strLogtext = "---------------------- New import  " & TextBox1.Text & " ---------------------------------" & vbCrLf
        TextBox2.Text = strLogtext & vbCrLf
        functions.logfile = logPath.Text & "\SASOP_LOG_" & Now().ToString.Replace(" ", "").Replace(":", "_") & ".txt"
        addtolog(functions.logfile, strLogtext)
        importstatus.Maximum = intNroflines
        importstatus.Minimum = 0

        ' Visa statusimport
        importstatus.Visible = True
        Label4.Visible = True
        Label5.Visible = True
        label6.Visible = True
        importstatus.Update()
        Label4.Update()
        Label5.Update()
        label6.Update()



        intOldorderid = 0
        intNrofrows = 0
        intActiveRow = 0
        While oRead.Peek <> -1
            intActiveRow = intActiveRow + 1
            importstatus.Value = intActiveRow

            label6.Text = intActiveRow & " / " & intNroflines
            label6.Update()

            strLineIn = oRead.ReadLine()
            arrData = strLineIn.Split("|")
            intOrderid = CInt(arrData(0))
            strKundNr = arrData(1)
            ' Check if the customer has nr 900000 and do not import these invoices
            If (strKundNr = "900000") Then
                strLogtext = Now() & "|" & TextBox1.Text & "| Row " & intActiveRow & "| CustomerNr 900000 detected - Invoice not imported" & vbCrLf
                addtolog(functions.logfile, strLogtext)
                TextBox2.Text = TextBox2.Text & strLogtext
                TextBox2.ScrollToCaret()
                TextBox2.Update()
            Else
                strLogtext = Now() & "|" & TextBox1.Text & "| Row " & intActiveRow & vbCrLf
                addtolog(functions.logfile, strLogtext)
                TextBox2.Text = TextBox2.Text & strLogtext
                TextBox2.ScrollToCaret()
                TextBox2.Update()

                'addtolog("c:\spcs\" & strLogfileName, strLogtext)


                strCountryCode = arrData(2)
                strCompany = Strings.Left(arrData(4), 50)
                strAddress1 = Strings.Left(arrData(5), 35)
                strAddress2 = Strings.Left(arrData(6), 35)
                strCity = arrData(8)
                strCountry = arrData(11)
                strPostalcode = arrData(10)
                strPaymentterms = arrData(12)
                strInvoicetype = arrData(13)
                strCurrency = arrData(14)
                dteOrderdate = arrData(15)
                If arrData(3) = "Y" Then
                    blnIntercompany = True
                Else
                    blnIntercompany = False
                End If

                intCounter = 0

                While (arrData(16 + 3 * intCounter) <> "")
                    ReDim Preserve arrArticle(intCounter)
                    strMomskod = arrData(16 + 3 * intCounter + 1)
                    If strMomskod = "Moms" Then
                        strVatCode = "1"
                    Else ' Tex Momsfritt, Export osv
                        strVatCode = "0"
                    End If

                    If (arrData(16 + 3 * intCounter) = "SERVICE") Then
                        strArticleName = "Tj�nst "
                        If strCountryCode = "SE" Then
                            If (strVatCode = "0") Then
                                ' Tj�nst - Svensk kund momsfritt
                                strArticleAccount = "3044"
                            Else
                                ' Tj�nst - Svensk kund moms 
                                strArticleAccount = "3041"
                            End If
                        ElseIf is_eu(strCountryCode) Then
                            If strVatCode = "1" Then
                                ' Tj�nst - Eu privatkund 
                                strArticleAccount = "3041"
                            Else
                                ' Tj�nst - Eu f�retagskund 
                                strArticleAccount = "3046"
                            End If
                        Else
                            If strVatCode = "1" Then
                                ' Tj�nst - Export privatkund 
                                strArticleAccount = "3041"
                            Else
                                ' Tj�nst - Export f�retagskund 
                                strArticleAccount = "3045"
                            End If
                        End If
                    Else
                        strArticleName = "Vara "
                        If strCountryCode = "SE" Then
                            If (strVatCode = "0") Then
                                ' Vara - Svensk kund momsfritt
                                strArticleAccount = "3054"
                            Else
                                ' Vara - Svensk kund momsfritt
                                strArticleAccount = "3051"
                            End If
                        ElseIf is_eu(strCountryCode) Then
                            If strVatCode = "1" Then
                                ' Vara - Eu privatkund 
                                strArticleAccount = "3051"
                            Else
                                ' Vara - Eu f�retagskund 
                                strArticleAccount = "3056"
                            End If
                        Else
                            If strVatCode = "1" Then
                                ' Vara - Export privatkund 
                                strArticleAccount = "3051"
                            Else
                                ' Vara - Export f�retagskund 
                                strArticleAccount = "3055"
                            End If
                        End If

                    End If

                    ' arrData(16 + 3 * intCounter + 2)


                    'This is to handle different locales
                    If (CDbl("1,23") = 123) Then
                        arrData(16 + 3 * intCounter + 2) = Replace(arrData(16 + 3 * intCounter + 2), ",", ".")

                    Else
                        arrData(16 + 3 * intCounter + 2) = Replace(arrData(16 + 3 * intCounter + 2), ".", ",")
                    End If



                    dblArticlePrice = CDbl(arrData(16 + 3 * intCounter + 2))

                    arrArticle(intCounter) = New String() {strArticleName, dblArticlePrice, strArticleAccount, strVatCode}
                    intCounter = intCounter + 1
                    If (intCounter = 3) Then
                        Exit While
                    End If
                End While
                ' Skapa faktura
                addinvoice(strMomskod, intOrderid, strKundNr, strCompany, strAddress1, strAddress2, strPostalcode, strCity, strCountry, dteOrderdate, strPaymentterms, strCurrency, strCountryCode, strInvoicetype, arrArticle)
            End If
            Application.DoEvents()
            ' Visa status p� importen
            label6.Text = intActiveRow.ToString() & " / " & intNroflines.ToString
            label6.Update()

        End While

        If functions.errFlag = True Then
            MsgBox("Orderimporten misslyckades!", MsgBoxStyle.Critical)
        Else
            MsgBox("Orderimporten lyckades!", MsgBoxStyle.Information)
        End If


        oRead.Close()

    End Sub


    Public Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        '        Dim privMyThread As New System.Threading.Thread(AddressOf importorders)
        '       functions.myThread = privMyThread
        '      functions.myThread.Start()
        importorders()
    End Sub



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'updatearticles("http://www.earstore.se/backweb/exportarticles.php", "starkey", "abc123")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'importorders("http://www.earstore.se/backweb/exportorders.php", "starkey", "abc123")

    End Sub






    Private Sub quitButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitButton.Click
        Dim Err As ADKERROR

        '        If (Not functions.myThread Is Nothing) Then
        ' functions.myThread.Abort()
        'End If
        If pdata <> 0 Then

            Err = AdkDeleteStruct(pdata)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
            End If
            Call AdkClose()
        End If
        Me.Close()
        End
    End Sub
End Class
