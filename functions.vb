Module functions
    'Deklarerar pekare
    Dim pdata As Long
    Public myThread As Threading.Thread
    Public logfile As String
    Public errFlag As Boolean = False



    Function addtolog(ByVal strFilename As String, ByVal strLog As String)
        Dim oFile As System.IO.File
        Dim oWrite As System.IO.StreamWriter
        oWrite = oFile.AppendText(strFilename)
        oWrite.WriteLine(strLog)
        oWrite.Close()
    End Function

    Public Function CountLinesFromFile(ByVal filename As String) As Integer
        Dim oFile As System.IO.File
        Dim result As String
        Dim sr As System.IO.StreamReader = Nothing
        Try
            Dim lineCount As Integer = 0
            sr = oFile.OpenText(filename)
            While Not sr.ReadLine() Is Nothing
                lineCount = lineCount + 1
            End While
            result = lineCount
            'result = lineCount.ToString()
        Catch ex As Exception
            result = ex.Message
        Finally
            If Not sr Is Nothing Then
                sr.Close()
            End If
        End Try
        Return result
    End Function


    ''' <summary>
    ''' Display possible errors from the VISMA integration module
    ''' </summary>
    ''' <param name="Err"></param>
    ''' <remarks></remarks>
    Public Sub GetErrorText(ByVal Err As ADKERROR)
        Dim strFunction As String
        Dim strDbTable As String
        Dim strProgramPart, strLogText As String
        Dim strField As String
        Dim strRc As String
        Dim intAnswer As Integer

        If Err.lFunction > 0 Then
            strFunction = Space(255)
            Call AdkGetErrorText(Err, 3, strFunction, 255)
            If InStr(strFunction, Chr(0)) Then strFunction = Microsoft.VisualBasic.Left(strFunction, InStr(strFunction, Chr(0)) - 1)
        End If

        If Err.lDbTable > 0 Then
            strDbTable = Space(255)
            Call AdkGetErrorText(Err, ADK_ERROR_TEXT_TYPE.elDbTable, strDbTable, 255)
            If InStr(strDbTable, Chr(0)) Then strDbTable = Microsoft.VisualBasic.Left(strDbTable, InStr(strDbTable, Chr(0)) - 1)
        End If

        If Err.lProgramPart > 0 Then
            strProgramPart = Space(255)
            Call AdkGetErrorText(Err, ADK_ERROR_TEXT_TYPE.elProgramPart, strProgramPart, 255)
            If InStr(strProgramPart, Chr(0)) Then strProgramPart = Microsoft.VisualBasic.Left(strProgramPart, InStr(strProgramPart, Chr(0)) - 1)
        End If

        If Err.lField > 0 Then
            strField = Space(255)
            Call AdkGetErrorText(Err, ADK_ERROR_TEXT_TYPE.elField, strField, 255)
            If InStr(strField, Chr(0)) Then strField = Microsoft.VisualBasic.Left(strField, InStr(strField, Chr(0)) - 1)
        End If

        If Err.lRc > 0 Then
            strRc = Space(255)
            Call AdkGetErrorText(Err, ADK_ERROR_TEXT_TYPE.elRc, strRc, 255)
            If InStr(strRc, Chr(0)) Then strRc = Microsoft.VisualBasic.Left(strRc, InStr(strRc, Chr(0)) - 1)
        End If

        intAnswer = MsgBox("Function Error:" & strFunction.ToString & vbCrLf & "DbTableError3: " & strDbTable & vbCrLf & strProgramPart & vbCrLf & "Field Error: " & strField & vbCrLf & "Rc Error: " & strRc & vbCrLf & "Vill du avbryta importen?", MsgBoxStyle.YesNo, "Fel i import!")
        If intAnswer = vbYes Then
            'myThread.Abort()
            If pdata <> 0 Then

                Err = AdkDeleteStruct(pdata)
                If Err.lRc > 0 Then
                    Call GetErrorText(Err)
                End If
            End If
            Call AdkClose()
            Application.Exit()
        End If
        strLogText = "Function Error:" & strFunction.ToString & vbCrLf & "DbTableError3: " & strDbTable & vbCrLf & strProgramPart & vbCrLf & "Field Error: " & strField & vbCrLf & "Rc Error: " & strRc & vbCrLf
        addtolog(logfile, strLogText)
        'Form1.TextBox2.Text = Form1.TextBox2.Text & vbCrLf & "Function Error:" & strFunction.ToString & vbCrLf & "DbTableError3: " & strDbTable & vbCrLf & strProgramPart & vbCrLf & "Field Error: " & strField & vbCrLf & "Rc Error: " & strRc & vbCrLf
        'Form1.TextBox2.ScrollToCaret()
        'Form1.TextBox2.Update()
        errFlag = True
    End Sub


    ''' <summary>
    ''' Checks if the country is within EU or not
    ''' </summary>
    ''' <param name="strCountrycode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function is_eu(ByVal strCountrycode As String) As Boolean
        Dim strEuarray() As String = {"AT", "BE", "CY", "CZ", "DK", "DE", "EE", "FI", "FR", "GB", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "PL", "PT", "SK", "SI", "ES", "SE", "NL", "UK"}
        Array.Sort(strEuarray)
        If (Array.BinarySearch(strEuarray, strCountrycode) >= 0) Then
            Return True
        Else
            Return False
        End If
    End Function





    Public Sub addinvoice(ByVal strMomskod As String, ByVal dblInvoiceNr As String, ByVal strKundnr As String, ByVal strCompany As String, ByVal strAddress1 As String, ByVal strAddress2 As String, ByVal strPostalcode As String, ByVal strCity As String, ByVal strCountry As String, ByVal dteOrderdate As Date, ByVal strPaymentterms As String, ByVal strCurrency As String, ByVal strCountryCode As String, ByVal strInvoicetype As String, ByVal arrArticle As String()())
        Dim Err As ADKERROR
        Dim intJuliandate As Integer
        Dim pRowData As Long
        Dim tmpRowData As Long
        Dim intNrofrows As Long
        Dim intCounter As Long
        Dim orderid As String
        Dim paymentdays As Double
        Dim dteExpiredate As Date
        Dim strArray() As String = strPaymentterms.Split(" ")

        paymentdays = strArray(0)
        dteExpiredate = dteOrderdate
        dteExpiredate = dteExpiredate.AddDays(paymentdays)


        'S�tt errorflaggan till false f�r eventuella felmeddelanden

        'Formatera kund/fakturanr att ligga p� serien 10000000
        'strKundnr = strKundnr.PadLeft(6, "0")
        'strKundnr = "1" & strKundnr

        'dblInvoiceNr = dblInvoiceNr.PadLeft(6, "0")
        'dblInvoiceNr = "1" & dblInvoiceNrwww.a


        'Skapa en ny kund med samma id som fakturan 
        addcustomer(strMomskod, strKundnr, strCompany, strAddress1, strAddress2, strPostalcode, strCity, strCountry, strCountryCode)

        ' Ber�kna antal rader p� fakturan
        intNrofrows = UBound(arrArticle, 1) + 1
        pdata = AdkCreateData(ADK_DB_INVOICE_HEAD)


        'L�gg till kundnummer
        Err = AdkSetStr(pdata, ADK_OOI_HEAD_CUSTOMER_NUMBER, strKundnr)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If








        'Kontrollera typ av faktura (Normal/Kredit)
        If strInvoicetype = "IH" Then
            strInvoicetype = "F"
        ElseIf strInvoicetype = "CH" Then
            strInvoicetype = "K"
            dteExpiredate = dteOrderdate
        End If
        Err = AdkSetStr(pdata, ADK_OOI_HEAD_TYPE_OF_INVOICE, strInvoicetype)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If




        ' Konvertera orderdatum till ett juliandate
        Err = AdkDateToLong(dteOrderdate, intJuliandate)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If

        ' S�tt orderdatum p� ordern
        Err = AdkSetDate(pdata, ADK_OOI_HEAD_DOCUMENT_DATE1, intJuliandate)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If


        'S�tt fakturanummer 
        Err = AdkSetDouble(pdata, ADK_OOI_HEAD_DOCUMENT_NUMBER, dblInvoiceNr)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If
        'AdkDeleteStruct(pdata)

        pRowData = AdkCreateDataRow(ADK_DB_INVOICE_ROW, intNrofrows)


        ' Iterera genom alla artikelrader
        For intCounter = 0 To intNrofrows - 1

            ' h�mta rad f�r rad
            tmpRowData = AdkGetDataRow(pRowData, intCounter)

            ' S�tt antal
            Err = AdkSetDouble(tmpRowData, ADK_OOI_ROW_QUANTITY2, 1)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Sub
            End If

            'S�tt(ben�mning)
            Err = AdkSetStr(tmpRowData, ADK_OOI_ROW_TEXT, arrArticle(intCounter)(0))
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Sub
            End If



            'S�tt kontonummer i SPCS
            Err = AdkSetStr(tmpRowData, ADK_OOI_ROW_ACCOUNT_NUMBER, arrArticle(intCounter)(2))
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Sub
            End If

            'S�tt momskod i SPCS
            Err = AdkSetStr(tmpRowData, ADK_OOI_ROW_VAT_CODE, arrArticle(intCounter)(3))
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Sub
            End If

            'S�tt(pris)
            Err = AdkSetDouble(tmpRowData, ADK_OOI_ROW_PRICE_EACH_CURRENT_CURRENCY, arrArticle(intCounter)(1))
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Sub
            End If



        Next

        ' Add the number of rows to the invoice
        Err = AdkSetDouble(pdata, ADK_OOI_HEAD_NROWS, intNrofrows)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If





        ' Add the actual rows
        Err = AdkSetData(pdata, ADK_OOI_HEAD_ROWS, pRowData)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If


        'S�tt fakturan till utskriven
        Err = AdkSetBool(pdata, ADK_OOI_HEAD_DOCUMENT_NOT_DONE, False)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If

        Err = AdkAdd(pdata)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        Else
        End If


        ' Konvertera f�rfallodag till ett(juliandate)
        Err = AdkDateToLong(dteExpiredate, intJuliandate)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If

        ' S�tt f�rfallodatum p� ordern
        Err = AdkSetDate(pdata, ADK_OOI_HEAD_DOCUMENT_DATE2, intJuliandate)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If




        'S�tt fakturan till utskriven
        Err = AdkSetBool(pdata, ADK_OOI_HEAD_DOCUMENT_PRINTED, True)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If


        Err = AdkUpdate(pdata)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If

        'Rensa minnesallokeringar f�r fakturan
        AdkDeleteStruct(pdata)
    End Sub

    Public Sub addarticle(ByVal strArticlenumber As String, ByVal strArticlename As String, ByVal strPrice As String)
        Dim Err As ADKERROR
        Dim blnUpdate As Boolean
        ' Skapa ett artikelobjekt


        pdata = AdkCreateData(ADK_DB_ARTICLE)


        ' S�tt artikelnumret
        Err = AdkSetStr(pdata, ADK_ARTICLE_NUMBER, strArticlenumber)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If


        blnUpdate = False

        'Kolla om artikeln redan finns i registret
        Err = AdkFind(pdata)
        If Err.lRc > 0 Then
            'Call GetErrorText(Err)
            'Exit Sub
        Else
            blnUpdate = True
        End If


        Dim strTmp As New System.Text.StringBuilder(strArticlename)
        ' Minska alla artikelnamn till 30 tecken
        If (strTmp.Length > 30) Then
            strTmp.Length = 30
            strArticlename = strTmp.ToString()
        End If

        ' S�tt artikelnamnet
        Err = AdkSetStr(pdata, ADK_ARTICLE_NAME, strArticlename)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If
        Err = AdkSetDouble(pdata, ADK_ARTICLE_PRICE, CInt(strPrice))
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If

        ' Check if the article should be added or updated
        If (blnUpdate) Then
            Err = AdkUpdate(pdata)
        Else
            Err = AdkAdd(pdata)
        End If

        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Sub
        End If
        Err = AdkDeleteStruct(pdata)


    End Sub

    ''' <summary>
    ''' Adds a customer to VISMA SPCS database
    ''' </summary>
    ''' <param name="strMomskod"></param>
    ''' <param name="strKundnr"></param>
    ''' <param name="strKundNamn"></param>
    ''' <param name="strAddress1"></param>
    ''' <param name="strAddress2"></param>
    ''' <param name="strPostalcode"></param>
    ''' <param name="strCity"></param>
    ''' <param name="strCountry"></param>
    ''' <param name="strCountryCode"></param>
    ''' <param name="strPhone"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function addcustomer(ByVal strMomskod As String, ByVal strKundnr As String, ByVal strKundNamn As String, ByVal strAddress1 As String, ByVal strAddress2 As String, ByVal strPostalcode As String, ByVal strCity As String, ByVal strCountry As String, ByVal strCountryCode As String, Optional ByVal strPhone As String = "") As String
        Dim Err As ADKERROR
        Dim intExport As Integer
        Dim intEu As Integer
        Dim boolUpdate = True

        ' Create a customer 
        pdata = AdkCreateData(ADK_DB_CUSTOMER)

        Err = AdkSetStr(pdata, ADK_CUSTOMER_NUMBER, strKundnr)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If


        ' Search for that customer
        Err = AdkFind(pdata)
        boolUpdate = False
        If Err.lRc = 0 Then
            boolUpdate = True
        Else
            Err = AdkSetStr(pdata, ADK_CUSTOMER_NUMBER, strKundnr)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Function
            End If

        End If



        Err = AdkSetStr(pdata, ADK_CUSTOMER_NAME, strKundNamn)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        ' Check the different VAT codes and set the correct settings for the accounting in VISMA accordingly
        If strMomskod = "Export" Then
            intExport = 1
            intEu = 0
        ElseIf strMomskod = "F�rs. tj�nster EU" Then
            intExport = 1
            intEu = 1
        ElseIf strMomskod = "F�rs. varor EU" Then
            intExport = 1
            intEu = 1
        ElseIf strMomskod = "Moms" Then
            intExport = 0
            intEu = 0
        ElseIf strMomskod = "Momsfritt" Then
            intExport = 0
            intEu = 0
        End If

        Err = AdkSetBool(pdata, ADK_CUSTOMER_EU_CUSTOMER, intEu)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        Err = AdkSetBool(pdata, ADK_CUSTOMER_EXPORT, intExport)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If



        Err = AdkSetStr(pdata, ADK_CUSTOMER_NAME, strKundNamn)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If
        'L�gg till kundens adressf�lt1 
        Err = AdkSetStr(pdata, ADK_CUSTOMER_MAILING_ADDRESS, strAddress1)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        If (strAddress2 <> "") Then
            Err = AdkSetStr(pdata, ADK_CUSTOMER_MAILING_ADDRESS2, strAddress2)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Function
            End If
        End If

        Err = AdkSetStr(pdata, ADK_CUSTOMER_ZIPCODE, strPostalcode)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        Err = AdkSetStr(pdata, ADK_CUSTOMER_CITY, strCity)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        Err = AdkSetStr(pdata, ADK_CUSTOMER_COUNTRY, strCountry)
        If Err.lRc > 0 Then
            Call GetErrorText(Err)
            Exit Function
        End If

        If boolUpdate Then
            ' Om det �r en gammal kund uppdatera eventuellt gamla uppgifter
            Err = AdkUpdate(pdata)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Function
            End If
        Else
            ' Om det �r en ny kund skapa denna
            Err = AdkAdd(pdata)
            If Err.lRc > 0 Then
                Call GetErrorText(Err)
                Exit Function
            End If
        End If
        Err = AdkDeleteStruct(pdata)


    End Function


    Public Function ConvertStringToByteArray(ByVal stringToConvert As String) As Byte()

        Return (New System.Text.ASCIIEncoding).GetBytes(stringToConvert)

    End Function


End Module
