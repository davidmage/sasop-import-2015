Module AdkApi

    Public Const ADKI_ADMIN2000 As Integer = 1
        Public Const ADKI_ADMIN1000 As Integer = 2
        Public Const ADKI_ADMIN500 As Integer = 3
        Public Const ADKI_FORENING As Integer = 4

        Public Const ADK_DB_CUSTOMER As Integer = 0
        Public Const ADK_DB_ARTICLE As Integer = 1
        Public Const ADK_DB_ORDER_HEAD As Integer = 2
        Public Const ADK_DB_ORDER_ROW As Integer = 3
        Public Const ADK_DB_OFFER_HEAD As Integer = 4
        Public Const ADK_DB_OFFER_ROW As Integer = 5
        Public Const ADK_DB_INVOICE_HEAD As Integer = 6
        Public Const ADK_DB_INVOICE_ROW As Integer = 7
        Public Const ADK_DB_SUPPLIER_INVOICE_HEAD As Integer = 8
        Public Const ADK_DB_SUPPLIER_INVOICE_ROW As Integer = 9
        Public Const ADK_DB_PROJECT As Integer = 10
        Public Const ADK_DB_ACCOUNT As Integer = 11
        Public Const ADK_DB_SUPPLIER As Integer = 12
        Public Const ADK_DB_CODE_OF_TERMS_OF_DELIVERY As Integer = 13
        Public Const ADK_DB_CODE_OF_WAY_OF_DELIVERY As Integer = 14
        Public Const ADK_DB_CODE_OF_TERMS_OF_PAYMENT As Integer = 15
        Public Const ADK_DB_CODE_OF_LANGUAGE As Integer = 16
        Public Const ADK_DB_CODE_OF_CURRENCY As Integer = 17
        Public Const ADK_DB_CODE_OF_CUSTOMER_CATEGORY As Integer = 18
        Public Const ADK_DB_CODE_OF_DISTRICT As Integer = 19
        Public Const ADK_DB_CODE_OF_SELLER As Integer = 20
        Public Const ADK_DB_DISCOUNT_AGREEMENT As Integer = 21
        Public Const ADK_DB_CODE_OF_ARTICLE_GROUP As Integer = 22
        Public Const ADK_DB_CODE_OF_ARTICLE_ACCOUNT As Integer = 23
        Public Const ADK_DB_CODE_OF_UNIT As Integer = 24
        Public Const ADK_DB_CODE_OF_PROFIT_CENTRE As Integer = 25
        Public Const ADK_DB_CODE_OF_PRICE_LIST As Integer = 26
        Public Const ADK_DB_PRM As Integer = 27
        Public Const ADK_DB_INVENTORY_ARTICLE As Integer = 28
        Public Const ADK_DB_MANUAL_DELIVERY_IN As Integer = 29
        Public Const ADK_DB_MANUAL_DELIVERY_OUT As Integer = 30
        Public Const ADK_DB_DISPATCHER As Integer = 31
        Public Const ADK_DB_BOOKING_HEAD As Integer = 32
        Public Const ADK_DB_BOOKING_ROW As Integer = 33
        Public Const ADK_DB_CODE_OF_CUSTOMER_DISCOUNT_ROW As Integer = 34
        Public Const ADK_DB_CODE_OF_ARTICLE_PARCEL As Integer = 35
        Public Const ADK_DB_CODE_OF_ARTICLE_NAME As Integer = 36
        Public Const ADK_DB_PRICE As Integer = 37
        Public Const ADK_DB_ARTICLE_PURCHASE_PRICE As Integer = 38
        Public Const ADK_DB_CODE_OF_WAY_OF_PAYMENT As Integer = 39
        Public Const ADK_DB_FREE_CATEGORY_1 As Integer = 40                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_2 As Integer = 41                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_3 As Integer = 42                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_4 As Integer = 43                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_5 As Integer = 44                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_6 As Integer = 45                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_7 As Integer = 46                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_8 As Integer = 47                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_9 As Integer = 48                     '//SPCS Förening
        Public Const ADK_DB_FREE_CATEGORY_10 As Integer = 49                    '//SPCS Förening
        Public Const ADK_DB_MEMBER As Integer = 50                              '//SPCS Förening
        Public Const ADK_DB_DELIVERY_NOTE_HEAD As Integer = 51
        Public Const ADK_DB_DELIVERY_NOTE_ROW As Integer = 52
        Public Const ADK_DB_PACKAGE_HEAD As Integer = 53
        Public Const ADK_DB_PACKAGE_ROW As Integer = 54
        Public Const ADK_DB_IMP_PACKAGE_HEAD As Integer = 55
        Public Const ADK_DB_IMP_PACKAGE_ROW As Integer = 56

        Public Const ADK_CUSTOMER_FIRST As Integer = 0
        Public Const ADK_CUSTOMER_NUMBER As Integer = 0                                                            'eChar
        Public Const ADK_CUSTOMER_NAME As Integer = 1                                                              'eChar
        Public Const ADK_CUSTOMER_SHORT_NAME As Integer = 2                                                        'eChar
        Public Const ADK_CUSTOMER_MAILING_ADDRESS As Integer = 3                                                   'eChar
        Public Const ADK_CUSTOMER_MAILING_ADDRESS2 As Integer = 4                                                  'eChar
        Public Const ADK_CUSTOMER_VISITING_ADDRESS As Integer = 5                                                  'eChar
        Public Const ADK_CUSTOMER_ZIPCODE As Integer = 6                                                           'eChar
        Public Const ADK_CUSTOMER_CITY As Integer = 7                                                              'eChar
        Public Const ADK_CUSTOMER_COUNTRY As Integer = 8                                                           'eChar
        Public Const ADK_CUSTOMER_TELEPHONE As Integer = 9                                                         'eChar
        Public Const ADK_CUSTOMER_TELEPHONE2 As Integer = 10                                                       'eChar
        Public Const ADK_CUSTOMER_FAX As Integer = 11                                                              'eChar
        Public Const ADK_CUSTOMER_DELIVERY_NAME As Integer = 12                                                    'eChar
        Public Const ADK_CUSTOMER_DELIVERY_ADDRESS As Integer = 13                                                 'eChar
        Public Const ADK_CUSTOMER_DELIVERY_ADDRESS2 As Integer = 14                                                'eChar
        Public Const ADK_CUSTOMER_DELIVERY_VISITING_ADDRESS As Integer = 15                                        'eChar
        Public Const ADK_CUSTOMER_DELIVERY_ZIPCODE As Integer = 16                                                 'eChar
        Public Const ADK_CUSTOMER_DELIVERY_CITY As Integer = 17                                                    'eChar
        Public Const ADK_CUSTOMER_DELIVERY_COUNTRY As Integer = 18                                                 'eChar
        Public Const ADK_CUSTOMER_DELIVERY_TELEPHONE As Integer = 19                                               'eChar
        Public Const ADK_CUSTOMER_DELIVERY_TELEPHONE2 As Integer = 20                                              'eChar
        Public Const ADK_CUSTOMER_DELIVERY_FAX As Integer = 21                                                     'eChar
        Public Const ADK_CUSTOMER_REFERENCE As Integer = 22                                                        'eChar
        Public Const ADK_CUSTOMER_BGIRO As Integer = 23                                                            'eChar
        Public Const ADK_CUSTOMER_PGIRO As Integer = 24                                                            'eChar
        Public Const ADK_CUSTOMER_ORGANISATION_NUMBER As Integer = 25                                              'eChar
        Public Const ADK_CUSTOMER_CATEGORY As Integer = 26                                                         'eChar
        Public Const ADK_CUSTOMER_DISTRICT As Integer = 27                                                         'eChar
        Public Const ADK_CUSTOMER_SELLER As Integer = 28                                                           'eChar
        Public Const ADK_CUSTOMER_CODE_OF_TERMS_OF_DELIVERY As Integer = 29                                        'eChar
        Public Const ADK_CUSTOMER_CODE_OF_TERMS_OF_PAYMENT As Integer = 30                                         'eChar
        Public Const ADK_CUSTOMER_CODE_OF_WAY_OF_DELIVERY As Integer = 31                                          'eChar
        Public Const ADK_CUSTOMER_REMINDER As Integer = 32                                                         'eBool
        Public Const ADK_CUSTOMER_DEMAND_FEE As Integer = 33                                                       'eBool
        Public Const ADK_CUSTOMER_INTEREST_INVOICE As Integer = 34                                                 'eBool
        Public Const ADK_CUSTOMER_DISPATCH_FEE As Integer = 35                                                     'eBool
        Public Const ADK_CUSTOMER_CARGO_FEE As Integer = 36                                                        'eBool
        Public Const ADK_CUSTOMER_CODE_OF_CURRENCY As Integer = 37                                                 'eChar
        Public Const ADK_CUSTOMER_CODE_OF_LANGUAGE As Integer = 38                                                 'eChar
        Public Const ADK_CUSTOMER_DISCOUNT_AGREEMENT As Integer = 39                                               'eChar
        Public Const ADK_CUSTOMER_NOTE_OF_OUTSTANDING_ORDERS As Integer = 40                                       'eBool
        Public Const ADK_CUSTOMER_EXPORT As Integer = 41                                                           'eBool
        Public Const ADK_CUSTOMER_LAST_INVOICE_DATE As Integer = 42                                                'eDate
        Public Const ADK_CUSTOMER_PRICE_LIST As Integer = 43                                                       'eChar
        Public Const ADK_CUSTOMER_INVOICE_DISCOUNT As Integer = 44                                                 'eDouble
        Public Const ADK_CUSTOMER_ROW_DISCOUNT As Integer = 45                                                     'eDouble
        Public Const ADK_CUSTOMER_REMARK1 As Integer = 46                                                          'eChar
        Public Const ADK_CUSTOMER_REMARK2 As Integer = 47                                                          'eChar
        Public Const ADK_CUSTOMER_CREDIT_LIMIT As Integer = 48                                                     'eDouble
        Public Const ADK_CUSTOMER_ACCUMULATE_TURNOVER_THIS_YEAR As Integer = 49                                    'eDouble
        Public Const ADK_CUSTOMER_ACCUMULATE_TURNOVER_LAST_YEAR As Integer = 50                                    'eDouble
        Public Const ADK_CUSTOMER_CONTRIBUTION_MARGIN_THIS_YEAR As Integer = 51                                    'eDouble
        Public Const ADK_CUSTOMER_CONTRIBUTION_MARGIN_LAST_YEAR As Integer = 52                                    'eDouble
        Public Const ADK_CUSTOMER_CLAIM As Integer = 53                                                            'eDouble
        Public Const ADK_CUSTOMER_AVERAGE_TIME_OF_PAYMENT_THIS_YEAR As Integer = 54                                'eDouble
        Public Const ADK_CUSTOMER_AVERAGE_TIME_OF_PAYMENT_THIS_YEAR_NO_P As Integer = 55                           'eDouble
        Public Const ADK_CUSTOMER_AVERAGE_TIME_OF_PAYMENT_LAST_YEAR As Integer = 56                                'eDouble
        Public Const ADK_CUSTOMER_AVERAGE_TIME_OF_PAYMENT_LAST_YEAR_NO_P As Integer = 57                           'eDouble
        Public Const ADK_CUSTOMER_SORT_ORDER As Integer = 58                                                       'eChar
        Public Const ADK_CUSTOMER_VAT_NUMBER As Integer = 59                                                       'eChar
        Public Const ADK_CUSTOMER_SUMMARY_INVOICE As Integer = 60                                                  'eBool
        Public Const ADK_CUSTOMER_EU_CUSTOMER As Integer = 61                                                      'eBool
        Public Const ADK_CUSTOMER_EMAIL As Integer = 62                                                            'eChar
        Public Const ADK_CUSTOMER_DOCUMENT_PATH As Integer = 63                                                    'eChar
        Public Const ADK_CUSTOMER_CONTRIBUTION_DEGREE As Integer = 64                                              'eDouble
        Public Const ADK_CUSTOMER_CONTRIBUTION_DEGREE_LAST_YEAR As Integer = 65                                    'eDouble
        Public Const ADK_CUSTOMER_LASTCHANGE As Integer = 66                                                       'eDate
        Public Const ADK_CUSTOMER_DISPATCHER As Integer = 67                                                       'eChar
        Public Const ADK_CUSTOMER_EANLOC As Integer = 68                                                           'eChar
        Public Const ADK_CUSTOMER_DELIVERY_EANLOC As Integer = 69                                                  'eChar
        Public Const ADK_CUSTOMER_CODE_OF_COUNTRY As Integer = 70                                                   '//eChar
        Public Const ADK_CUSTOMER_DELIVERY_CODE_OF_COUNTRY As Integer = 71                                          '//eChar
        Public Const ADK_CUSTOMER_FIRST_NAME As Integer = 72                                                        '//eChar     //Föreningsspecifikt
        Public Const ADK_CUSTOMER_MEMBER As Integer = 73                                                            '//eBool     //Föreningsspecifikt
        Public Const ADK_CUSTOMER_LAST As Integer = 73

        Public Const ADK_ARTICLE_FIRST As Integer = 0
        Public Const ADK_ARTICLE_NUMBER As Integer = 0                                                             'eChar
        Public Const ADK_ARTICLE_NAME As Integer = 1                                                               'eChar
        Public Const ADK_ARTICLE_NAME_X As Integer = 2                                                             'eChar
        Public Const ADK_ARTICLE_SHORT_NAME As Integer = 3                                                         'eChar
        Public Const ADK_ARTICLE_GROUP As Integer = 4                                                              'eChar
        Public Const ADK_ARTICLE_ACCOUNTING_CODE As Integer = 5                                                    'eChar
        Public Const ADK_ARTICLE_STOCK_GOODS As Integer = 6                                                        'eBool
        Public Const ADK_ARTICLE_UNIT_CODE As Integer = 7                                                          'eChar
        Public Const ADK_ARTICLE_SORT_ORDER As Integer = 8                                                         'eChar
        Public Const ADK_ARTICLE_QUANTITY_IN_STOCK As Integer = 9                                                  'eDouble
        Public Const ADK_ARTICLE_QUANTITY_RESERVED As Integer = 10                                                 'eDouble
        Public Const ADK_ARTICLE_ORDER_POINT As Integer = 11                                                       'eDouble
        Public Const ADK_ARTICLE_MINIMUM_ORDER As Integer = 12                                                     'eDouble
        Public Const ADK_ARTICLE_MAXIMUM_ORDER As Integer = 13                                                     'eDouble
        Public Const ADK_ARTICLE_ORDERED_QUANTITY As Integer = 14                                                  'eDouble
        Public Const ADK_ARTICLE_TIME_OF_DELIVERY As Integer = 15                                                  'eDouble
        Public Const ADK_ARTICLE_PLACE_IN_STOCK As Integer = 16                                                    'eChar
        Public Const ADK_ARTICLE_WEIGHT As Integer = 17                                                            'eDouble
        Public Const ADK_ARTICLE_VOLUME As Integer = 18                                                            'eDouble
        Public Const ADK_ARTICLE_SUPPLIER_NUMBER As Integer = 19                                                   'eChar
        Public Const ADK_ARTICLE_INVOICE_DATE As Integer = 20                                                      'eDate
        Public Const ADK_ARTICLE_INVENTORY_DATE As Integer = 21                                                    'eDate
        Public Const ADK_ARTICLE_INVENTORY_QUANTITY As Integer = 22                                                'eDouble
        Public Const ADK_ARTICLE_INVENTORY_DIFFERENCE As Integer = 23                                              'eDouble
        Public Const ADK_ARTICLE_INVENTORY_ACCUMULATE As Integer = 24                                              'eDouble
        Public Const ADK_ARTICLE_LAST_PURCHASE_PRICE As Integer = 25                                               'eDouble
        Public Const ADK_ARTICLE_NOTE_OF_OUTSTANDING_ARTICLES As Integer = 26                                      'eBool
        Public Const ADK_ARTICLE_LAND_OF_ORIGIN As Integer = 27                                                    'eChar
        Public Const ADK_ARTICLE_EXPORT_CODE As Integer = 28                                                       'eChar
        Public Const ADK_ARTICLE_REMARK1 As Integer = 29                                                           'eChar
        Public Const ADK_ARTICLE_REMARK2 As Integer = 30                                                           'eChar
        Public Const ADK_ARTICLE_DOCUMENT_PATH As Integer = 31                                                     'eChar
        Public Const ADK_ARTICLE_ACCUMULATED_TURNOVER_THIS_YEAR As Integer = 32                                    'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_TURNOVER_LAST_YEAR As Integer = 33                                    'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_THIS_YEAR As Integer = 34                                    'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_LAST_YEAR As Integer = 35                                    'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_THIS_YEAR As Integer = 36                                     'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_LAST_YEAR As Integer = 37                                     'eDouble
        Public Const ADK_ARTICLE_CLOSING_FLAG As Integer = 38                                                      'eBool
        Public Const ADK_ARTICLE_ESTIMATED_PURCHASE_PRICE As Integer = 39                                          'eDouble
        Public Const ADK_ARTICLE_ESTIMATED_CARGO_FEE As Integer = 40                                               'eDouble
        Public Const ADK_ARTICLE_ESTIMATED_OTHER As Integer = 41                                                   'eDouble
        Public Const ADK_ARTICLE_STOCK_VALUE As Integer = 42                                                       'eDouble
        Public Const ADK_ARTICLE_STOCK_VALUE_INACTIVE As Integer = 43                                              'eBool
        Public Const ADK_ARTICLE_PRICE_LIST As Integer = 44                                                        'eChar
        Public Const ADK_ARTICLE_PRICE As Integer = 45                                                             'eDouble
        Public Const ADK_ARTICLE_ESTIMATED_TOTAL_PURCHASE_PRICE As Integer = 46                                    'eDouble
        Public Const ADK_ARTICLE_DISPOSABLE_IN_STOCK As Integer = 47                                               'eDouble
        Public Const ADK_ARTICLE_TURNOVER_RATE As Integer = 48                                                     'eDouble
        Public Const ADK_ARTICLE_ACCUMULATE_TURNOVER_PARCEL_THIS_YEAR As Integer = 49                              'eDouble
        Public Const ADK_ARTICLE_ACCUMULATE_TURNOVER_TOTAL_THIS_YEAR As Integer = 50                               'eDouble
        Public Const ADK_ARTICLE_ACCUMULATE_TURNOVER_PARCEL_LAST_YEAR As Integer = 51                              'eDouble
        Public Const ADK_ARTICLE_ACCUMULATE_TURNOVER_TOTAL_LAST_YEAR As Integer = 52                               'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_PARCEL_THIS_YEAR As Integer = 53                             'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_TOTAL_THIS_YEAR As Integer = 54                              'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_PARCEL_LAST_YEAR As Integer = 55                             'eDouble
        Public Const ADK_ARTICLE_ACCUMULATED_QUANTITY_TOTAL_LAST_YEAR As Integer = 56                              'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_PARCEL_THIS_YEAR As Integer = 57                              'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_TOTAL_THIS_YEAR As Integer = 58                               'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_PARCEL_LAST_YEAR As Integer = 59                              'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_MARGIN_TOTAL_LAST_YEAR As Integer = 60                               'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_THIS_YEAR As Integer = 61                                     'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_PARCEL_THIS_YEAR As Integer = 62                              'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_TOTAL_THIS_YEAR As Integer = 63                               'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_LAST_YEAR As Integer = 64                                     'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_PARCEL_LAST_YEAR As Integer = 65                              'eDouble
        Public Const ADK_ARTICLE_CONTRIBUTION_DEGREE_TOTAL_LAST_YEAR As Integer = 66                               'eDouble
        Public Const ADK_ARTICLE_BAR_CODE As Integer = 67                                                          'eChar
        Public Const ADK_ARTICLE_BAR_CODE_TYPE As Integer = 68                                                     'eDoubl
        Public Const ADK_ARTICLE_PARCEL As Integer = 69                                                            'eBool
        Public Const ADK_ARTICLE_TYPE As Integer = 70              '//eChar     //Föreningsspecifikt       
        Public Const ADK_ARTICLE_WEBSHOP As Integer = 71              '//eBool
        Public Const ADK_ARTICLE_IMAGE_PATH As Integer = 72              '//eChar    
        Public Const ADK_ARTICLE_LAST As Integer = 72


        Public Const ADK_OOI_HEAD_FIRST As Integer = 0
        Public Const ADK_OOI_HEAD_DOCUMENT_NUMBER As Integer = 0                                           'eDouble
        Public Const ADK_OOI_HEAD_DOCUMENT_CONNECTION_NUMBER1 As Integer = 1                               'eDouble
        Public Const ADK_OOI_HEAD_DOCUMENT_CONNECTION_NUMBER2 As Integer = 2                               'eDouble
        Public Const ADK_OOI_HEAD_DOCUMENT_CONNECTION_NUMBER3 As Integer = 3                               'eDouble
        Public Const ADK_OOI_HEAD_DOCUMENT_CONNECTION_NUMBER4 As Integer = 4                               'eDouble
        Public Const ADK_OOI_HEAD_DOCUMENT_NOT_DONE As Integer = 5                                         'eBool
        Public Const ADK_OOI_HEAD_DOCUMENT_PRINTED As Integer = 6                                          'eBool
        Public Const ADK_OOI_HEAD_DOCUMENT_CANCELLED As Integer = 7                                        'eBool
        Public Const ADK_OOI_HEAD_DOCUMENT_DATE1 As Integer = 8                                            'eDate
        Public Const ADK_OOI_HEAD_DOCUMENT_DATE2 As Integer = 9                                            'eDate
        Public Const ADK_OOI_HEAD_ORDER_DELIVERED As Integer = 10                                          'eBool
        Public Const ADK_OOI_HEAD_ORDER_PICK_PRINTED As Integer = 11                                       'eBool
        Public Const ADK_OOI_HEAD_ORDER_DELIVERY_NOTE_PRINTED As Integer = 12                              'eBool
        Public Const ADK_OOI_HEAD_TYPE_OF_INVOICE As Integer = 13                                          'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_NUMBER As Integer = 14                                          'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_NAME As Integer = 15                                            'eChar
        Public Const ADK_OOI_HEAD_MAILING_ADDRESS1 As Integer = 16                                         'eChar
        Public Const ADK_OOI_HEAD_MAILING_ADDRESS2 As Integer = 17                                         'eChar
        Public Const ADK_OOI_HEAD_ZIPCODE As Integer = 18                                                  'eChar
        Public Const ADK_OOI_HEAD_CITY As Integer = 19                                                     'eChar
        Public Const ADK_OOI_HEAD_COUNTRY As Integer = 20                                                  'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_NAME As Integer = 21                                            'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_MAILING_ADDRESS1 As Integer = 22                                'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_MAILING_ADDRESS2 As Integer = 23                                'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_ZIPCODE As Integer = 24                                         'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_CITY As Integer = 25                                            'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_COUNTRY As Integer = 26                                         'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_REFERENCE_NAME As Integer = 27                                  'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_ORDER_NUMBER As Integer = 28                                    'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_REFERENCE_NUMBER As Integer = 29                                'eChar
        Public Const ADK_OOI_HEAD_OUR_REFERENCE_NAME As Integer = 30                                       'eChar
        Public Const ADK_OOI_HEAD_DISTRICT_CODE As Integer = 31                                            'eChar
        Public Const ADK_OOI_HEAD_SELLER_CODE As Integer = 32                                              'eChar
        Public Const ADK_OOI_HEAD_CURRENCY_CODE As Integer = 33                                            'eChar
        Public Const ADK_OOI_HEAD_LANGUAGE_CODE As Integer = 34                                            'eChar
        Public Const ADK_OOI_HEAD_CURRENCY_RATE As Integer = 35                                            'eDouble
        Public Const ADK_OOI_HEAD_CURRENCY_UNIT As Integer = 36                                            'eDouble
        Public Const ADK_OOI_HEAD_REMINDER As Integer = 37                                                 'eBool
        Public Const ADK_OOI_HEAD_DEMAND_FEE As Integer = 38                                               'eBool
        Public Const ADK_OOI_HEAD_INVOICE_INTEREST As Integer = 39                                         'eBool
        Public Const ADK_OOI_HEAD_EXPORT As Integer = 40                                                   'eBool
        Public Const ADK_OOI_HEAD_INCLUDING_VAT As Integer = 41                                            'eBool
        Public Const ADK_OOI_HEAD_DO_NOT_CREATE_BACKORDER As Integer = 42                                  'eBool
        Public Const ADK_OOI_HEAD_LIST_OF_PRICE_CODE As Integer = 43                                       'eChar
        Public Const ADK_OOI_HEAD_INVOICE_DISCOUNT As Integer = 44                                         'eDouble
        Public Const ADK_OOI_HEAD_CODE_OF_DISCOUNT As Integer = 45                                         'eChar
        Public Const ADK_OOI_HEAD_CODE_OF_TERMS_OF_PAYMENT As Integer = 46                                 'eChar
        Public Const ADK_OOI_HEAD_CODE_OF_TERMS_OF_DELIVERY As Integer = 47                                'eChar
        Public Const ADK_OOI_HEAD_CODE_OF_WAY_OF_DELIVERY As Integer = 48                                  'eChar
        Public Const ADK_OOI_HEAD_TEXT1 As Integer = 49                                                    'eChar
        Public Const ADK_OOI_HEAD_TEXT2 As Integer = 50                                                    'eChar
        Public Const ADK_OOI_HEAD_TEXT3 As Integer = 51                                                    'eChar
        Public Const ADK_OOI_HEAD_SET_OF_VAT1 As Integer = 52                                              'eDouble
        Public Const ADK_OOI_HEAD_SET_OF_VAT2 As Integer = 53                                              'eDouble
        Public Const ADK_OOI_HEAD_SET_OF_VAT3 As Integer = 54                                              'eDouble
        Public Const ADK_OOI_HEAD_SET_OF_VAT4 As Integer = 55                                              'eDouble
        Public Const ADK_OOI_HEAD_CARGO_AMOUNT As Integer = 56                                             'eDouble
        Public Const ADK_OOI_HEAD_CARGO_VAT_CODE As Integer = 57                                           'eChar
        Public Const ADK_OOI_HEAD_DISPATCH_FEE As Integer = 58                                             'eDouble
        Public Const ADK_OOI_HEAD_VAT_CODE_DISPATCH_FEE As Integer = 59                                    'eChar
        Public Const ADK_OOI_HEAD_ROUND_OFF As Integer = 60                                                'eDouble
        Public Const ADK_OOI_HEAD_TOTAL_AMOUNT As Integer = 61                                             'eDouble
        Public Const ADK_OOI_HEAD_BALANCE As Integer = 62                                                  'eDouble
        Public Const ADK_OOI_HEAD_PROJECT_CODE As Integer = 63                                             'eChar
        Public Const ADK_OOI_HEAD_PROFIT_CENTRE As Integer = 64                                            'eChar
        Public Const ADK_OOI_HEAD_BASIS_OF_VAT0 As Integer = 65                                            'eDouble
        Public Const ADK_OOI_HEAD_BASIS_OF_VAT1 As Integer = 66                                            'eDouble
        Public Const ADK_OOI_HEAD_BASIS_OF_VAT2 As Integer = 67                                            'eDouble
        Public Const ADK_OOI_HEAD_BASIS_OF_VAT3 As Integer = 68                                            'eDouble
        Public Const ADK_OOI_HEAD_BASIS_OF_VAT4 As Integer = 69                                            'eDouble
        Public Const ADK_OOI_HEAD_AMOUNT_OF_VAT1 As Integer = 70                                           'eDouble
        Public Const ADK_OOI_HEAD_AMOUNT_OF_VAT2 As Integer = 71                                           'eDouble
        Public Const ADK_OOI_HEAD_AMOUNT_OF_VAT3 As Integer = 72                                           'eDouble
        Public Const ADK_OOI_HEAD_AMOUNT_OF_VAT4 As Integer = 73                                           'eDouble
        Public Const ADK_OOI_HEAD_PENALTY_INTEREST_RATE As Integer = 74                                    'eDouble
        Public Const ADK_OOI_HEAD_CUSTOMER_DEMAND_ACCOUNT As Integer = 75                                  'eChar
        Public Const ADK_OOI_HEAD_CARGO_ACCOUNT As Integer = 76                                            'eChar
        Public Const ADK_OOI_HEAD_DISPATCH_FEE_ACCOUNT As Integer = 77                                     'eChar
        Public Const ADK_OOI_HEAD_BALANCE2 As Integer = 78                                                 'eDouble
        'Public Const ADK_OOI_HEAD_CREDIT_NOTE_STORAGE_AFFECT            As Integer = 79                    'eBool
        Public Const ADK_OOI_HEAD_NUMBER_OF_ORIGINAl_INVOICE As Integer = 80                               'eDouble
        Public Const ADK_OOI_HEAD_VAT1_ACCOUNT As Integer = 81                                             'eChar
        Public Const ADK_OOI_HEAD_VAT2_ACCOUNT As Integer = 82                                             'eChar
        Public Const ADK_OOI_HEAD_VAT3_ACCOUNT As Integer = 83                                             'eChar
        Public Const ADK_OOI_HEAD_VAT4_ACCOUNT As Integer = 84                                             'eChar
        Public Const ADK_OOI_HEAD_ROUND_OFF_ACCOUNT As Integer = 85                                        'eChar
        Public Const ADK_OOI_HEAD_DISCOUNT_ACCOUNT As Integer = 86                                         'eChar
        Public Const ADK_OOI_HEAD_CUSTOMER_INVOICE_VAT_NUMBER As Integer = 87                              'eChar
        Public Const ADK_OOI_HEAD_SUMMARY_INVOICE As Integer = 88                                          'eBool
        Public Const ADK_OOI_HEAD_EU_ACCOUNT_FOR_QUARTERLY As Integer = 89                                 'eBool
        Public Const ADK_OOI_HEAD_EU_THIRD_PART_TRADE As Integer = 90                                      'eBool
        Public Const ADK_OOI_HEAD_INVOICE_WORK_BY_CONTRACT_SENT As Integer = 91                            'eBool
        Public Const ADK_OOI_HEAD_INVOICE_WORK_BY_CONTRACT_DONE As Integer = 92                            'eBool
        Public Const ADK_OOI_HEAD_SAVE_TEXT As Integer = 93                                                'eBool
        Public Const ADK_OOI_HEAD_EXTRA_DOCUMENT_PRINTED As Integer = 94                                   'eBool
        Public Const ADK_OOI_HEAD_BOOK_ORDER As Integer = 95                                               'eBool
        Public Const ADK_OOI_HEAD_GROSS As Integer = 96                                                    'eDouble
        Public Const ADK_OOI_HEAD_NET As Integer = 97                                                      'eDouble
        Public Const ADK_OOI_HEAD_EXCLUDING_OF_VAT As Integer = 98                                         'eDouble
        Public Const ADK_OOI_HEAD_VAT_AMOUNT As Integer = 99                                               'eDouble
        Public Const ADK_OOI_HEAD_CONTRIBUTION_MARGIN As Integer = 100                                     'eDouble
        Public Const ADK_OOI_HEAD_VALUE As Integer = 101                                                   'eDouble
        Public Const ADK_OOI_HEAD_TYPE_IN_TEXT As Integer = 102                                            'eChar
        Public Const ADK_OOI_HEAD_ROWS As Integer = 103                                                   'eData
        Public Const ADK_OOI_HEAD_NROWS As Integer = 104                                                  'eDouble
        Public Const ADK_OOI_HEAD_LOCAL_REMARK As Integer = 105                                           'eChar
        Public Const ADK_OOI_HEAD_CREDIT_INVOICE_COPY_NUMBER As Integer = 106                              'eDouble
        Public Const ADK_OOI_HEAD_CREDIT_INVOICE_COPY_WHAT As Integer = 107                                'eDouble
        Public Const ADK_OOI_HEAD_EANLOC As Integer = 108                                                  'eChar
        Public Const ADK_OOI_HEAD_DELIVERY_EANLOC As Integer = 109                                         'eChar
        Public Const ADK_OOI_HEAD_DISPATCHER As Integer = 110                                              'eChar
        Public Const ADK_OOI_HEAD_CREDIT_NOTE_STORAGE_AFFECT As Integer = 111                              'eBool
        Public Const ADK_OOI_HEAD_OCR_NUMBER As Integer = 112              '//eChar
        Public Const ADK_OOI_HEAD_CODE_OF_COUNTRY As Integer = 113              '//eChar
        Public Const ADK_OOI_HEAD_DELIVERY_CODE_OF_COUNTRY As Integer = 114              '//eChar
        Public Const ADK_OOI_HEAD_INVOICE_BASE As Integer = 115              '//eDouble
        Public Const ADK_OOI_HEAD_CONTRACTNR As Integer = 116              '//eChar
        Public Const ADK_OOI_HEAD_TOT_SHIPWT As Integer = 117              '//eDouble
        Public Const ADK_OOI_HEAD_LAST As Integer = 117

        Public Const ADK_OOI_ROW_FIRST As Integer = 0
        Public Const ADK_OOI_ROW_ARTICLE_NUMBER As Integer = 0                                             'eChar
        Public Const ADK_OOI_ROW_SUPPLIER_ARTICLE_NUMBER As Integer = 1                                    'eChar
        Public Const ADK_OOI_ROW_QUANTITY1 As Integer = 2                                                     'eDouble
        Public Const ADK_OOI_ROW_QUANTITY2 As Integer = 3                                                 'eDouble
        Public Const ADK_OOI_ROW_QUANTITY3 As Integer = 4                                                     'eDouble
        Public Const ADK_OOI_ROW_PRICE_EACH_CURRENT_CURRENCY As Integer = 5                                'eDouble
        Public Const ADK_OOI_ROW_PRICE2 As Integer = 6                                                        'eDouble
        Public Const ADK_OOI_ROW_DISCOUNT As Integer = 7                                                   'eDouble
        Public Const ADK_OOI_ROW_AMOUNT_DISCOUNT_FLAG As Integer = 8                                       'eBool
        Public Const ADK_OOI_ROW_AMOUNT_CURRENT_CURRENCY As Integer = 9                                       'eDouble
        Public Const ADK_OOI_ROW_AMOUNT_DOMESTIC_CURRENCY As Integer = 10                                        'eDouble
        Public Const ADK_OOI_ROW_ACCOUNT_NUMBER As Integer = 11                                                       'eChar
        Public Const ADK_OOI_ROW_PROFIT_CENTRE As Integer = 12                                            'eChar
        Public Const ADK_OOI_ROW_PROJECT As Integer = 13                                                          'eChar
        Public Const ADK_OOI_ROW_TEXT As Integer = 14                                                         'eChar
        Public Const ADK_OOI_ROW_UNIT As Integer = 15                                                             'eChar
        Public Const ADK_OOI_ROW_VAT_CODE As Integer = 16                                                     'eChar
        Public Const ADK_OOI_ROW_NOTE_OF_OUTSTANDING_ORDERS As Integer = 17                               'eBool
        Public Const ADK_OOI_ROW_DATE2 As Integer = 18                                                        'eDate
        Public Const ADK_OOI_ROW_TYPE_OF_ROW As Integer = 19                                                     'eChar
        Public Const ADK_OOI_ROW_CONTRIBUTION_DEGREE As Integer = 20                                          'eChar
        Public Const ADK_OOI_ROW_CONTRIBUTION_MARGIN As Integer = 21                                          'eDouble
        Public Const ADK_OOI_ROW_PRINT As Integer = 22                                                    'eDouble
        Public Const ADK_OOI_ROW_PRINT2 As Integer = 23                                                    'eDouble
        'Public Const ADK_OOI_ROW_CONTRIBUTION_MARGIN                    As Integer = 24                    'eDouble
        Public Const ADK_OOI_ROW_BAR_CODE As Integer = 25                                                  'eChar
        Public Const ADK_OOI_ROW_DATE3 As Integer = 26              '//eDate                             
        Public Const ADK_OOI_ROW_ARTICLE_TYPE As Integer = 27              '//eChar     //Föreningsspecifikt
        Public Const ADK_OOI_ROW_FROM_TYPE As Integer = 28              '//eDouble 
        Public Const ADK_OOI_ROW_FROM_DOCUMENT As Integer = 29              '//eDouble 
        Public Const ADK_OOI_ROW_FROM_DOCROW As Integer = 30              '//eChar
        Public Const ADK_OOI_ROW_ROWNUMBER As Integer = 31              '//eDouble
        Public Const ADK_OOI_ROW_CONNECTION_TYPE As Integer = 32             ' //eChar 
        Public Const ADK_OOI_ROW_CONNECTION_DOCUMENT As Integer = 33             ' //eChar   
        Public Const ADK_OOI_ROW_CONNECTION_DOCROW As Integer = 34             ' //eChar   
        Public Const ADK_OOI_ROW_SERIAL_NUMBER As Integer = 35             ' //eChar   
        Public Const ADK_OOI_ROW_BATCH_NUMBER As Integer = 36             ' //eChar   
        Public Const ADK_OOI_ROW_BEST_BEFORE As Integer = 37             ' //eDate                             
        Public Const ADK_OOI_ROW_NOTIFY_QT1 As Integer = 38             ' //eDouble   
        Public Const ADK_OOI_ROW_NOTIFY_QT2 As Integer = 39             ' //eDouble           
        Public Const ADK_OOI_ROW_LAST As Integer = 39


        Public Const ADK_SUP_INV_HEAD_FIRST As Integer = 0
        Public Const ADK_SUP_INV_HEAD_GIVEN_NUMBER As Integer = 0                                          'eDouble
        Public Const ADK_SUP_INV_HEAD_SUPPLIER_NUMBER As Integer = 1                                       'eChar
        Public Const ADK_SUP_INV_HEAD_SUPPLIER_NAME As Integer = 2                                                  'eChar
        Public Const ADK_SUP_INV_HEAD_PGIRO As Integer = 3                                                 'eChar
        Public Const ADK_SUP_INV_HEAD_ORGANISATION_NUMBER As Integer = 4                                   'eChar
        Public Const ADK_SUP_INV_HEAD_BGIRO As Integer = 5                                                    'eChar
        Public Const ADK_SUP_INV_HEAD_INVOICE_NUMBER As Integer = 6                                        'eChar
        Public Const ADK_SUP_INV_HEAD_CUSTOMER_NUMBER As Integer = 7                                       'eChar
        Public Const ADK_SUP_INV_HEAD_INVOICE_DATE As Integer = 8                                          'eDate
        Public Const ADK_SUP_INV_HEAD_DUE_DATE As Integer = 9                                              'eDate
        Public Const ADK_SUP_INV_HEAD_CURRENCY_CODE As Integer = 10                                        'eChar
        Public Const ADK_SUP_INV_HEAD_CURRENCY_RATE As Integer = 11                                        'eDouble
        Public Const ADK_SUP_INV_HEAD_TYPE_OF_INVOICE As Integer = 12                                      'eChar
        Public Const ADK_SUP_INV_HEAD_TOTAL As Integer = 13                                                   'eDouble
        Public Const ADK_SUP_INV_HEAD_BALANCE_CURRENT_CURRENCY As Integer = 14                             'eDouble
        Public Const ADK_SUP_INV_HEAD_OUTGOING_PAYMENTS_CURRENT_CURRENCY As Integer = 15                  'eDouble
        Public Const ADK_SUP_INV_HEAD_VAT_AMOUNT As Integer = 16                                         'eDouble
        Public Const ADK_SUP_INV_HEAD_CARGO_FEE As Integer = 17                                          'eDouble
        Public Const ADK_SUP_INV_HEAD_NAME_OF_CERTIFIER As Integer = 18                                     'eChar
        Public Const ADK_SUP_INV_HEAD_CERTIFIED As Integer = 19                                            'eBool
        Public Const ADK_SUP_INV_HEAD_CANCELLED As Integer = 20                                            'eBool
        Public Const ADK_SUP_INV_HEAD_PROJECT As Integer = 21                                               'eChar
        Public Const ADK_SUP_INV_HEAD_PROFIT_CENTRE As Integer = 22                                         'eChar
        Public Const ADK_SUP_INV_HEAD_JOURNAL_NUMBER As Integer = 23                                        'eDouble
        Public Const ADK_SUP_INV_HEAD_NOTE1 As Integer = 24                                                 'eChar
        Public Const ADK_SUP_INV_HEAD_NOTE2 As Integer = 25                                                 'eChar
        Public Const ADK_SUP_INV_HEAD_NOTE3 As Integer = 26                                                 'eChar
        Public Const ADK_SUP_INV_HEAD_CURRENCY_UNIT As Integer = 27                                        'eDouble
        Public Const ADK_SUP_INV_HEAD_SUPPLIER_DEBT_ACCOUNT As Integer = 28                                  'eChar
        Public Const ADK_SUP_INV_HEAD_CODE_OF_CENTRAL_BANK As Integer = 29                                'eChar
        Public Const ADK_SUP_INV_HEAD_PAY_WITH_CURRENCY_ACCOUNT As Integer = 30                            'eBool
        Public Const ADK_SUP_INV_HEAD_PAYMENT_ABROAD As Integer = 31                                       'eBool
        Public Const ADK_SUP_INV_HEAD_CODE_OF_CHARGE_ON_PAYMENT_ABROAD As Integer = 32                     'eDouble
        Public Const ADK_SUP_INV_HEAD_TYPE_IN_TEXT As Integer = 33                                       'eChar
        Public Const ADK_SUP_INV_HEAD_VERIFICATION_SERIES_ID As Integer = 34                             'eChar
        Public Const ADK_SUP_INV_HEAD_CHANGE_OF_STOCK As Integer = 35                                       'eData
        Public Const ADK_SUP_INV_HEAD_ROWS As Integer = 36
        Public Const ADK_SUP_INV_HEAD_NROWS As Integer = 37                                                 'eDouble
        Public Const ADK_SUP_INV_HEAD_CREDIT_INVOICE_COPY_NUMBER As Integer = 38                           'eDouble
        Public Const ADK_SUP_INV_HEAD_CREDIT_INVOICE_COPY_WHAT As Integer = 39                             'eDouble
        Public Const ADK_SUP_INV_HEAD_CREDIT_NUMBER As Integer = 40                                            'eChar
        Public Const ADK_SUP_INV_HEAD_LAST As Integer = 40


        Public Const ADK_PROJECT_FIRST As Integer = 0
        Public Const ADK_PROJECT_CODE_OF_PROJECT As Integer = 0                                          'eChar
        Public Const ADK_PROJECT_NAME As Integer = 1                                                     'eChar
        Public Const ADK_PROJECT_DATE_OF_BEGINNING As Integer = 2                                        'eDate
        Public Const ADK_PROJECT_DATE_OF_END As Integer = 3                                              'eDate
        Public Const ADK_PROJECT_DATE_OF_CLOSURE As Integer = 4                                          'eDate
        Public Const ADK_PROJECT_RESPONSIBLE_OF_PROJECT As Integer = 5                                   'eChar
        Public Const ADK_PROJECT_CUSTOMER_NUMBER As Integer = 6                                          'eChar
        Public Const ADK_PROJECT_CUSTOMER_NAME As Integer = 7                                            'eChar
        Public Const ADK_PROJECT_OUR_ORDER_NUMBER As Integer = 8                                         'eDouble
        Public Const ADK_PROJECT_CUSTOMER_ORDER_NUMBER As Integer = 9                                    'eChar
        Public Const ADK_PROJECT_CUSTOMER_REFERENCE_NAME As Integer = 10                                 'eChar
        Public Const ADK_PROJECT_TELEPHONE As Integer = 11                                               'eChar
        Public Const ADK_PROJECT_FAX As Integer = 12                                                     'eChar
        Public Const ADK_PROJECT_NOTE_ABOUT_WORKPLACE As Integer = 13                                    'eChar
        Public Const ADK_PROJECT_NOTE_ABOUT_WORKPLACE2 As Integer = 14                                   'eChar
        Public Const ADK_PROJECT_NOTE1 As Integer = 15                                                   'eChar
        Public Const ADK_PROJECT_NOTE2 As Integer = 16                                                   'eChar
        Public Const ADK_PROJECT_NOTE3 As Integer = 17                                                   'eChar
        Public Const ADK_PROJECT_NOTE4 As Integer = 18                                                   'eChar
        Public Const ADK_PROJECT_CHART_OF_ACCOUNTS_TYPE As Integer = 19                                  'eChar
        Public Const ADK_PROJECT_CLOSED As Integer = 20                                                  'eBool
        Public Const ADK_PROJECT_LAST As Integer = 20


        Public Const ADK_ACCOUNT_FIRST As Integer = 0
        Public Const ADK_ACCOUNT_SEASON_ID As Integer = 0                                                  'eChar
        Public Const ADK_ACCOUNT_NUMBER As Integer = 1                                                     'eChar
        Public Const ADK_ACCOUNT_TEXT As Integer = 2                                                       'eChar
        Public Const ADK_ACCOUNT_VAT_CODE As Integer = 3                                                   'eChar
        Public Const ADK_ACCOUNT_SRU_CODE As Integer = 4                                                   'eChar
        Public Const ADK_ACCOUNT_DEBIT_CREDIT As Integer = 5                                               'eChar
        Public Const ADK_ACCOUNT_AUTOMATION_DEVIDE As Integer = 6                                          'eChar
        Public Const ADK_ACCOUNT_MANUAL_CODING As Integer = 7                                              'eBool
        Public Const ADK_ACCOUNT_PROFIT_CENTRE_ON_ACCOUNT As Integer = 8                                   'eChar
        Public Const ADK_ACCOUNT_PROFIT_CENTRE As Integer = 9                                              'eChar
        Public Const ADK_ACCOUNT_PROJECT_ON_ACCOUNT As Integer = 10                                        'eChar
        Public Const ADK_ACCOUNT_PROJECT As Integer = 11                                                   'eChar
        Public Const ADK_ACCOUNT_SUB_ACCOUNT As Integer = 12                                               'eChar
        Public Const ADK_ACCOUNT_SUB_ACCOUNT_DEFINITION As Integer = 13                                    'eChar
        Public Const ADK_ACCOUNT_QUANTITY_UNIT_ON_ACCOUNT As Integer = 14                                  'eChar
        Public Const ADK_ACCOUNT_QUANTITY_UNIT As Integer = 15                                             'eChar
        Public Const ADK_ACCOUNT_QUANTITY_UNIT_NORMAL As Integer = 16                                      'eChar
        Public Const ADK_ACCOUNT_ROW_INFO_ON_ACCOUNT As Integer = 17                                       'eChar
        Public Const ADK_ACCOUNT_ROW_INFO As Integer = 18                                                  'eChar
        Public Const ADK_ACCOUNT_TYPE_OF_ACCOUNT As Integer = 19                                           'eChar
        Public Const ADK_ACCOUNT_NOT_ACTIVE As Integer = 20                                                'eBool
        Public Const ADK_ACCOUNT_NEVER_DETAILED As Integer = 21                                            'eBool
        Public Const ADK_ACCOUNT_LAST As Integer = 21


        Public Const ADK_SUPPLIER_FIRST As Integer = 0
        Public Const ADK_SUPPLIER_NUMBER As Integer = 0                                                 'eChar
        Public Const ADK_SUPPLIER_NAME As Integer = 1                                                   'eChar
        Public Const ADK_SUPPLIER_SHORT_NAME As Integer = 2                                             'eChar
        Public Const ADK_SUPPLIER_MAILING_ADDRESS As Integer = 3                                        'eChar
        Public Const ADK_SUPPLIER_MAILING_ADDRESS2 As Integer = 4                                       'eChar
        Public Const ADK_SUPPLIER_VISITING_ADDRESS As Integer = 5                                       'eChar
        Public Const ADK_SUPPLIER_ZIPCODE As Integer = 6                                                'eChar
        Public Const ADK_SUPPLIER_CITY As Integer = 7                                                   'eChar
        Public Const ADK_SUPPLIER_COUNTRY As Integer = 8                                                'eChar
        Public Const ADK_SUPPLIER_TELEPHONE As Integer = 9                                              'eChar
        Public Const ADK_SUPPLIER_TELEPHONE2 As Integer = 10                                            'eChar
        Public Const ADK_SUPPLIER_FAX As Integer = 11                                                   'eChar
        Public Const ADK_SUPPLIER_REFERENCE As Integer = 12                                             'eChar
        Public Const ADK_SUPPLIER_OUR_REFERENCE As Integer = 13                                         'eChar
        Public Const ADK_SUPPLIER_ORGANISATION_NUMBER As Integer = 14                                   'eChar
        Public Const ADK_SUPPLIER_CODE_OF_CURRENCY As Integer = 15                                      'eChar
        Public Const ADK_SUPPLIER_CODE_OF_LANGUAGE As Integer = 16                                      'eChar
        Public Const ADK_SUPPLIER_BGIRO As Integer = 17                                                 'eChar
        Public Const ADK_SUPPLIER_PGIRO As Integer = 18                                                 'eChar
        Public Const ADK_SUPPLIER_OUR_CUSTOMER_NUMBER As Integer = 19                                   'eChar
        Public Const ADK_SUPPLIER_CREDIT_LIMIT As Integer = 20                                          'eDouble
        Public Const ADK_SUPPLIER_DEBT As Integer = 21                                                  'eDouble
        Public Const ADK_SUPPLIER_PAYMENT_SENT As Integer = 22                                          'eDouble
        Public Const ADK_SUPPLIER_LAST_INVOICE_DATE As Integer = 23                                     'eDate
        Public Const ADK_SUPPLIER_ACCUMULATE_TURNOVER_THIS_YEAR As Integer = 24                         'eDouble
        Public Const ADK_SUPPLIER_ACCUMULATE_TURN_OVER_LAST_YEAR As Integer = 25                        'eDouble
        Public Const ADK_SUPPLIER_CODE_OF_TERMS_OF_PAYMENT As Integer = 26                              'eChar
        Public Const ADK_SUPPLIER_CODE_OF_TERMS_OF_DELIVERY As Integer = 27                             'eChar
        Public Const ADK_SUPPLIER_CODE_OF_WAY_OF_DELIVERY As Integer = 28                               'eChar
        Public Const ADK_SUPPLIER_REMARK1 As Integer = 29                                               'eChar
        Public Const ADK_SUPPLIER_REMARK2 As Integer = 30                                               'eChar
        Public Const ADK_SUPPLIER_SORT_ORDER As Integer = 31                                            'eChar
        Public Const ADK_SUPPLIER_CODE_OF_COUNTRY As Integer = 32                                       'eChar
        Public Const ADK_SUPPLIER_CODE_OF_CENTRAL_BANK As Integer = 33                                  'eChar
        Public Const ADK_SUPPLIER_RECIEVER_NUMBER_OF_BANK As Integer = 34                               'eDouble
        Public Const ADK_SUPPLIER_SWIFT_ADDRESS As Integer = 35                                         'eChar
        Public Const ADK_SUPPLIER_CODE_OF_BANK As Integer = 36                                          'eChar
        Public Const ADK_SUPPLIER_NAME_OF_BANK As Integer = 37                                          'eChar
        Public Const ADK_SUPPLIER_ADDRESS_OF_BANK As Integer = 38                                       'eChar
        Public Const ADK_SUPPLIER_ZIPCODE_OF_BANK As Integer = 39                                       'eChar
        Public Const ADK_SUPPLIER_CITY_OF_BANK As Integer = 40                                          'eBool
        Public Const ADK_SUPPLIER_PAYMENT_ABROAD As Integer = 41                                        'eDouble
        Public Const ADK_SUPPLIER_BG_CODE_OF_FEE_FOR_PAYMENT_ABROAD As Integer = 42                     'eChar
        Public Const ADK_SUPPLIER_EMAIL As Integer = 43                                                 'eChar
        Public Const ADK_SUPPLIER_WWW As Integer = 44                                                   'eChar
        Public Const ADK_SUPPLIER_DOCUMENT As Integer = 45                                              'eChar
        Public Const ADK_SUPPLIER_OCR_SUPPLIER As Integer = 46                                          'eBool
        Public Const ADK_SUPPLIER_CURRENCY_ACCOUNT As Integer = 47                                      'eBool
        Public Const ADK_SUPPLIER_EANLOC As Integer = 48                                                'eChar
        Public Const ADK_SUPPLIER_EU_PAYMENT_SEK_EUR As Integer = 49              '//eBool
        Public Const ADK_SUPPLIER_LAST As Integer = 49

        Public Const ADK_CODE_OF_TERMS_OF_DELIVERY_FIRST As Integer = 0
        Public Const ADK_CODE_OF_TERMS_OF_DELIVERY_CODE As Integer = 0                                  'eChar
        Public Const ADK_CODE_OF_TERMS_OF_DELIVERY_TEXT As Integer = 1                                  'eChar
        Public Const ADK_CODE_OF_TERMS_OF_DELIVERY_LAST As Integer = 1

        Public Const ADK_CODE_OF_WAY_OF_DELIVERY_FIRST As Integer = 0
        Public Const ADK_CODE_OF_WAY_OF_DELIVERY_CODE As Integer = 0                                    'eChar
        Public Const ADK_CODE_OF_WAY_OF_DELIVERY_TEXT As Integer = 1                                    'eChar
        Public Const ADK_CODE_OF_WAY_OF_DELIVERY_LAST As Integer = 1

        Public Const ADK_CODE_OF_TERMS_OF_PAYMENT_FIRST As Integer = 0
        Public Const ADK_CODE_OF_TERMS_OF_PAYMENT_CODE As Integer = 0                                   'eChar
        Public Const ADK_CODE_OF_TERMS_OF_PAYMENT_TEXT As Integer = 1                                   'eChar
        Public Const ADK_CODE_OF_TERMS_OF_PAYMENT_PAYMENT_CODE As Integer = 2                           'eChar
        Public Const ADK_CODE_OF_TERMS_OF_PAYMENT_LAST As Integer = 2

        Public Const ADK_CODE_OF_LANGUAGE_FIRST As Integer = 0
        Public Const ADK_CODE_OF_LANGUAGE_CODE As Integer = 0                                           'eChar
        Public Const ADK_CODE_OF_LANGUAGE_TEXT As Integer = 1                                           'eChar
        Public Const ADK_CODE_OF_LANGUAGE_LAST As Integer = 1

        Public Const ADK_CODE_OF_CURRENCY_FIRST As Integer = 0
        Public Const ADK_CODE_OF_CURRENCY_CODE As Integer = 0                                           'eChar
        Public Const ADK_CODE_OF_CURRENCY_COUNTRY As Integer = 1                                        'eChar
        Public Const ADK_CODE_OF_CURRENCY_UNIT As Integer = 2                                           'eDouble
        Public Const ADK_CODE_OF_CURRENCY_DATE_OF_REGISTRATION As Integer = 3                           'eDate
        Public Const ADK_CODE_OF_CURRENCY_SELL As Integer = 4                                           'eDouble
        Public Const ADK_CODE_OF_CURRENCY_BUY As Integer = 5                                            'eDouble
        Public Const ADK_CODE_OF_CURRENCY_LAST As Integer = 5

        Public Const ADK_CODE_OF_CUSTOMER_CATEGORY_FIRST As Integer = 0
        Public Const ADK_CODE_OF_CUSTOMER_CATEGORY_CODE As Integer = 0                                  'eChar
        Public Const ADK_CODE_OF_CUSTOMER_CATEGORY_TEXT As Integer = 1                                  'eChar
        Public Const ADK_CODE_OF_CUSTOMER_CATEGORY_LAST As Integer = 1

        Public Const ADK_CODE_OF_DISTRICT_FIRST As Integer = 0
        Public Const ADK_CODE_OF_DISTRICT_CODE As Integer = 0                                           'eChar
        Public Const ADK_CODE_OF_DISTRICT_TEXT As Integer = 1                                           'eChar
        Public Const ADK_CODE_OF_DISTRICT_LAST As Integer = 1

        Public Const ADK_CODE_OF_SELLER_FIRST As Integer = 0
        Public Const ADK_CODE_OF_SELLER_SIGN As Integer = 0                                             'eChar
        Public Const ADK_CODE_OF_SELLER_NAME As Integer = 1                                             'eChar
        Public Const ADK_CODE_OF_SELLER_LAST As Integer = 1

        Public Const ADK_DISCOUNT_AGREEMENT_FIRST As Integer = 0
        Public Const ADK_DISCOUNT_AGREEMENT_CODE As Integer = 0                                         'eChar
        Public Const ADK_DISCOUNT_AGREEMENT_TEXT As Integer = 1                                         'eChar
        Public Const ADK_DISCOUNT_AGREEMENT_PRICE_LIST As Integer = 2                                   'eChar
        Public Const ADK_DISCOUNT_AGREEMENT_INVOICE_DISCOUNT As Integer = 3                             'eDouble
        Public Const ADK_DISCOUNT_AGREEMENT_ROW_DISCOUNT As Integer = 4                                 'eDouble
        Public Const ADK_DISCOUNT_AGREEMENT_LAST As Integer = 4

        Public Const ADK_CODE_OF_ARTICLE_GROUP_FIRST As Integer = 0
        Public Const ADK_CODE_OF_ARTICLE_GROUP_CODE As Integer = 0                                      'eChar
        Public Const ADK_CODE_OF_ARTICLE_GROUP_TEXT As Integer = 1                                      'eChar
        Public Const ADK_CODE_OF_ARTICLE_GROUP_LAST As Integer = 1

        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_FIRST As Integer = 0
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_CODE As Integer = 0                                    'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_TEXT As Integer = 1                                    'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_SALES As Integer = 2                                   'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_EXPORT_SALES As Integer = 3                            'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_SALES_EU As Integer = 4                                'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_PURCHASE As Integer = 5                                'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_STOCK As Integer = 6                                   'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_STOCK_CHANGE As Integer = 7                            'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_PROFIT_CENTRE As Integer = 8                           'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_VAT As Integer = 9                                     'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_VAT_EU As Integer = 10                                 'eChar
        Public Const ADK_CODE_OF_ARTICLE_ACCOUNT_LAST As Integer = 10

        Public Const ADK_CODE_OF_UNIT_FIRST As Integer = 0
        Public Const ADK_CODE_OF_UNIT_CODE As Integer = 0                                               'eChar
        Public Const ADK_CODE_OF_UNIT_DOMESTIC As Integer = 1                                           'eBool
        Public Const ADK_CODE_OF_UNIT_LANGUAGE As Integer = 2                                           'eChar
        Public Const ADK_CODE_OF_UNIT_TEXT As Integer = 3                                               'eChar
        Public Const ADK_CODE_OF_UNIT_DECIMALS As Integer = 4                                          'eDouble
        Public Const ADK_CODE_OF_UNIT_LAST As Integer = 4

        Public Const ADK_CODE_OF_PROFIT_CENTRE_FIRST As Integer = 0
        Public Const ADK_CODE_OF_PROFIT_CENTRE_YEAR_ID As Integer = 0                                   'eChar
        Public Const ADK_CODE_OF_PROFIT_CENTRE_CODE As Integer = 1                                      'eChar
        Public Const ADK_CODE_OF_PROFIT_CENTRE_NAME As Integer = 2                                      'eChar
        Public Const ADK_CODE_OF_PROFIT_CENTRE_LAST As Integer = 2

        Public Const ADK_CODE_OF_PRICE_LIST_FIRST As Integer = 0
        Public Const ADK_CODE_OF_PRICE_LIST_CODE As Integer = 0                                            'eChar
        Public Const ADK_CODE_OF_PRICE_LIST_TEXT As Integer = 1                                         'eChar
        Public Const ADK_CODE_OF_PRICE_LIST_CURRENCY_CODE As Integer = 2                                'eChar
        Public Const ADK_CODE_OF_PRICE_LIST_LANGUAGE_CODE As Integer = 3                                'eChar
        Public Const ADK_CODE_OF_PRICE_LIST_INCLUDING_VAT As Integer = 4                                'eBool
        Public Const ADK_CODE_OF_PRICE_LIST_NOTE As Integer = 5                                          'eChar
        Public Const ADK_CODE_OF_PRICE_LIST_CHANGE_DATE As Integer = 6                                  'eDate
        Public Const ADK_CODE_OF_PRICE_LIST_INTERVAL1 As Integer = 7                                    'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_INTERVAL2 As Integer = 8                                    'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_INTERVAL3 As Integer = 9                                    'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_INTERVAL4 As Integer = 10                                   'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_ROUND_OFF1 As Integer = 11                                 'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_ROUND_OFF2 As Integer = 12                                  'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_ROUND_OFF3 As Integer = 13                                 'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_ROUND_OFF4 As Integer = 14                                 'eDouble
        Public Const ADK_CODE_OF_PRICE_LIST_LAST As Integer = 14

        Public Const ADK_PRM_FIRST As Integer = 0
        Public Const ADK_PRM_NAME As Integer = 0                                                               'eChar
        Public Const ADK_PRM_MAILING_ADDRESS As Integer = 1                                             'eChar
        Public Const ADK_PRM_VISITING_ADDRESS As Integer = 2                                            'eChar
        Public Const ADK_PRM_ZIPCODE As Integer = 3                                                        'eChar
        Public Const ADK_PRM_CITY As Integer = 4                                                               'eChar
        Public Const ADK_PRM_COUNTRY As Integer = 5                                                            'eChar
        Public Const ADK_PRM_TELEPHONE As Integer = 6                                                      'eChar
        Public Const ADK_PRM_TELEPHONE2 As Integer = 7                                                     'eChar
        Public Const ADK_PRM_FAX As Integer = 8                                                                    'eChar
        Public Const ADK_PRM_BGIRO As Integer = 9                                                              'eChar
        Public Const ADK_PRM_PGIRO As Integer = 10                                                             'eChar
        Public Const ADK_PRM_ORGANISATION_NUMBER As Integer = 11                                               'eChar
        Public Const ADK_PRM_VAT_REGISTER_NUMBER As Integer = 12                                               'eChar
        Public Const ADK_PRM_VAT_NUMBER As Integer = 13                                                    'eChar
        Public Const ADK_PRM_CODE_OF_LANGUAGE As Integer = 14                                           'eChar
        Public Const ADK_PRM_CODE_OF_CURRENCY As Integer = 15                                           'eChar
        Public Const ADK_PRM_CODE_OF_BRANCH As Integer = 16                                             'eChar
        Public Const ADK_PRM_EMAIL As Integer = 17                                                         'eChar
        Public Const ADK_PRM_WWW As Integer = 18                                                                   'eChar
        Public Const ADK_PRM_SEAT As Integer = 19                                                          'eChar
        Public Const ADK_PRM_DELIVERY_NAME As Integer = 20                                                 'eChar
        Public Const ADK_PRM_DELIVERY_ADDRESS As Integer = 21                                           'eChar
        Public Const ADK_PRM_DELIVERY_ADDRESS2 As Integer = 22                                          'eChar
        Public Const ADK_PRM_DELIVERY_ZIPCODE As Integer = 23                                           'eChar
        Public Const ADK_PRM_DELIVERY_CITY As Integer = 24                                                 'eChar
        Public Const ADK_PRM_DELIVERY_MAILING_ADDRESS As Integer = 25                                   'eChar
        Public Const ADK_PRM_DELIVERY_COUNTRY As Integer = 26                                          'eChar
        Public Const ADK_PRM_DELIVERY_TELEPHONE As Integer = 27                                        'eChar
        Public Const ADK_PRM_DELIVERY_REFERENCE As Integer = 28                                         'eChar
        Public Const ADK_PRM_EANLOC As Integer = 29                                                  'eChar
        Public Const ADK_PRM_DELIVERY_EANLOC As Integer = 30                                         'eChar
        Public Const ADK_PRM_IBAN_NUMBER As Integer = 31                                             'eChar
        Public Const ADK_PRM_SWIFT_ADRESS As Integer = 32                                            'eChar
        Public Const ADK_PRM_BANK_NAME As Integer = 33                                               'eChar
        Public Const ADK_PRM_BANK_ADRESS As Integer = 34                                             'eChar
        Public Const ADK_PRM_BANK_ADRESS2 As Integer = 35                                            'eChar
        Public Const ADK_PRM_BANK_ZIPCODE As Integer = 36                                            'eChar
        Public Const ADK_PRM_BANK_CITY As Integer = 37                                               'eChar
        Public Const ADK_PRM_CODE_OF_COUNTRY As Integer = 38                                         'eChar
        Public Const ADK_PRM_ACCOUNT_NUMBER As Integer = 39                                          'eChar
        Public Const ADK_PRM_ADDRESS_CODE_OF_COUNTRY As Integer = 40              '//eChar
        Public Const ADK_PRM_DELIVERY_CODE_OF_COUNTRY As Integer = 41              '//eChar
        Public Const ADK_PRM_LAST As Integer = 41

        Public Const ADK_INVENTORY_ARTICLE_FIRST As Integer = 0
        Public Const ADK_INVENTORY_ARTICLE_ARTICLE_NUMBER As Integer = 0                           'eChar
        Public Const ADK_INVENTORY_ARTICLE_ARTICLE_NAME As Integer = 1                             'eChar
        Public Const ADK_INVENTORY_ARTICLE_ARTICLE_GROUP As Integer = 2                            'eChar
        Public Const ADK_INVENTORY_ARTICLE_ARTICLE_PLACE_IN_STOCK As Integer = 3                   'eChar
        Public Const ADK_INVENTORY_ARTICLE_ARTICLE_UNIT_CODE As Integer = 4                        'eChar
        Public Const ADK_INVENTORY_ARTICLE_QUANTITY As Integer = 5                                 'eDouble
        Public Const ADK_INVENTORY_ARTICLE_DATE As Integer = 6                                     'eDate
        Public Const ADK_INVENTORY_ARTICLE_QUANTITY_IN_STOCK As Integer = 7                        'eDouble
        Public Const ADK_INVENTORY_ARTICLE_DIFFERENCE As Integer = 8                               'eChar
        Public Const ADK_INVENTORY_ARTICLE_LAST_DATE As Integer = 9                                'eDate
        Public Const ADK_INVENTORY_ARTICLE_LAST_DIFFERENCE As Integer = 10                         'eDouble
        Public Const ADK_INVENTORY_ARTICLE_ACCUMULATED_DIFFERENCE As Integer = 11                  'eDouble
        Public Const ADK_INVENTORY_ARTICLE_FLAG As Integer = 12                                    'eBool
        Public Const ADK_INVENTORY_ARTICLE_LAST As Integer = 12

        Public Const ADK_MANUAL_DELIVERY_IN_FIRST As Integer = 0
        Public Const ADK_MANUAL_DELIVERY_IN_DOCUMENT_NUMBER As Integer = 0                         'eDouble
        Public Const ADK_MANUAL_DELIVERY_IN_DATE As Integer = 1                                    'eDate
        Public Const ADK_MANUAL_DELIVERY_IN_ARTICLE_NUMBER As Integer = 2                          'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_ARTICLE_NAME As Integer = 3                            'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_UNIT As Integer = 4                                    'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_QUANTITY As Integer = 5                                'eDouble
        Public Const ADK_MANUAL_DELIVERY_IN_PRICE_EACH As Integer = 6                              'eDouble
        Public Const ADK_MANUAL_DELIVERY_IN_CARGO_AMOUNT_EACH As Integer = 7                       'eDouble
        Public Const ADK_MANUAL_DELIVERY_IN_TEXT As Integer = 8                                    'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_ACCOUNT As Integer = 9                                 'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_PROFIT_CENTRE As Integer = 10                          'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_PROJECT As Integer = 11                                'eChar
        Public Const ADK_MANUAL_DELIVERY_IN_CANCELLED As Integer = 12                              'eBool
        Public Const ADK_MANUAL_DELIVERY_IN_JOURNAL_PRINTED As Integer = 13                        'eBool
        Public Const ADK_MANUAL_DELIVERY_IN_LAST As Integer = 13

        Public Const ADK_MANUAL_DELIVERY_OUT_FIRST As Integer = 0
        Public Const ADK_MANUAL_DELIVERY_OUT_DOCUMENT_NUMBER As Integer = 0                        'eDouble
        Public Const ADK_MANUAL_DELIVERY_OUT_DATE As Integer = 1                                   'eDate
        Public Const ADK_MANUAL_DELIVERY_OUT_ARTICLE_NUMBER As Integer = 2                         'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_ARTICLE_NAME As Integer = 3                           'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_UNIT As Integer = 4                                   'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_QUANTITY As Integer = 5                               'eDouble
        Public Const ADK_MANUAL_DELIVERY_OUT_TEXT As Integer = 6                                   'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_ACCOUNT As Integer = 7                                'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_PROFIT_CENTRE As Integer = 8                          'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_PROJECT As Integer = 9                                'eChar
        Public Const ADK_MANUAL_DELIVERY_OUT_CANCELLED As Integer = 10                             'eBool
        Public Const ADK_MANUAL_DELIVERY_OUT_JOURNAL_PRINTED As Integer = 11                       'eBool
        Public Const ADK_MANUAL_DELIVERY_OUT_LAST As Integer = 11

        Public Const ADK_DISPATCHER_FIST As Integer = 0
        Public Const ADK_DISPATCHER_ID As Integer = 0                                              'eChar
        Public Const ADK_DISPATCHER_TEXT As Integer = 1                                            'eChar
        Public Const ADK_DISPATCHER_OUR_CUSTOMER_NUMBER As Integer = 2                             'eChar
        Public Const ADK_DISPATCHER_LAST As Integer = 2

        Public Const ADK_BOOKING_FIRST As Integer = 0
        Public Const ADK_BOOKING_DOCUMENT_NUMBER As Integer = 0              '//eDouble
        Public Const ADK_BOOKING_SUPPLIER_NUMBER As Integer = 1              '//eChar	
        Public Const ADK_BOOKING_SUPPLIER_NAME As Integer = 2              '//eChar	
        Public Const ADK_BOOKING_MAILING_ADDRESS As Integer = 3              '//eChar	
        Public Const ADK_BOOKING_MAILING_ADDRESS2 As Integer = 4              '//eChar	
        Public Const ADK_BOOKING_ZIPCODE As Integer = 5              '//eChar	
        Public Const ADK_BOOKING_CITY As Integer = 6                '//eChar	
        Public Const ADK_BOOKING_COUNTRY As Integer = 7              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_NAME As Integer = 8              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_MAILING_ADDRESS1 As Integer = 9              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_MAILING_ADDRESS2 As Integer = 10              '//eChar	
        Public Const ADK_BOOKING_VISITING_ADDRESS As Integer = 11              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_ZIPCODE As Integer = 12              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_CITY As Integer = 13              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_COUNTRY As Integer = 14              '//eChar	
        Public Const ADK_BOOKING_SUPPLIER_CUSTOMER_REFERENCE_NAME As Integer = 15              '//eChar	
        Public Const ADK_BOOKING_SUPPLIER_REFERENCE As Integer = 16              '//eChar	
        Public Const ADK_BOOKING_OUR_REFERENCE As Integer = 17              '//eChar	
        Public Const ADK_BOOKING_LANGUAGE_CODE As Integer = 18              '//eChar	
        Public Const ADK_BOOKING_CURRENCY_CODE As Integer = 19              '//eChar	
        Public Const ADK_BOOKING_SUPPLIER_ORDER_NUMBER As Integer = 20              '//eChar	
        Public Const ADK_BOOKING_NOT_DONE As Integer = 21              '//eBool	
        Public Const ADK_BOOKING_DELIVERED As Integer = 22              '//eBool	
        Public Const ADK_BOOKING_CANCELLED As Integer = 23              '//eBool	
        Public Const ADK_BOOKING_PRINTED As Integer = 24              '//eBool	
        Public Const ADK_BOOKING_CODE_OF_TERMS_OF_PAYMENT As Integer = 25              '//eChar	
        Public Const ADK_BOOKING_CODE_OF_TERMS_OF_DELIVERY As Integer = 26              '//eChar	
        Public Const ADK_BOOKING_CODE_OF_WAY_OF_DELIVERY As Integer = 27              '//eChar	
        Public Const ADK_BOOKING_DATE As Integer = 28              '//eDate	
        Public Const ADK_BOOKING_DELIVERY_DATE As Integer = 29              '//eDate	
        Public Const ADK_BOOKING_PROJECT_CODE As Integer = 30              '//eChar	
        Public Const ADK_BOOKING_PROFIT_CENTRE As Integer = 31              '//eChar	
        Public Const ADK_BOOKING_TOTAL_AMOUNT As Integer = 32              '//eDouble
        Public Const ADK_BOOKING_TEXT1 As Integer = 33              '//eChar	
        Public Const ADK_BOOKING_TEXT2 As Integer = 34              '//eChar	
        Public Const ADK_BOOKING_TEXT3 As Integer = 35              '//eChar	
        Public Const ADK_BOOKING_OUR_CUSTOMER_NUMBER As Integer = 36              '//eChar	
        Public Const ADK_BOOKING_EANLOC As Integer = 37              '//eChar	
        Public Const ADK_BOOKING_DELIVERY_EANLOC As Integer = 38              '//eChar	
        Public Const ADK_BOOKING_ROWS As Integer = 39              '//eData   
        Public Const ADK_BOOKING_NROWS As Integer = 40              '//eDouble 
        Public Const ADK_BOOKING_CODE_OF_COUNTRY As Integer = 41              '//eChar
        Public Const ADK_BOOKING_DELIVERY_CODE_OF_COUNTRY As Integer = 42              '//eChar
        Public Const ADK_BOOKING_CONTRACTNR As Integer = 43              '//eChar
        Public Const ADK_BOOKING_LAST As Integer = 43

        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_FIRST As Integer = 0              '//eChar
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_CODE As Integer = 0              '//eBool	
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_ARTICLE_GROUP_FLAG As Integer = 1              '//eChar	
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_ARTICLE_GROUP As Integer = 2              '//eChar
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_ARTICLE_NUMBER As Integer = 3              '//eChar	
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_PRICELIST As Integer = 4              '//eDouble
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_ROW_DISCOUNT As Integer = 5
        Public Const ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_LAST As Integer = 5

        Public Const ADK_CODE_OF_ARTICLE_PARCEL_FIRST As Integer = 0
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_NR As Integer = 0              ''//eChar	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_ARTICLE_NR As Integer = 1              '//eChar	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_TEXT As Integer = 2              '//eChar	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_TEXT2 As Integer = 3              '//eChar	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_QUANTITY As Integer = 4              '//eDouble
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_TOTAL As Integer = 5              '//eDouble
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_ROWNR As Integer = 6              '//eDouble
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_TYPE_OF_ROW As Integer = 7              '//eChar	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_WRITE_OFFER As Integer = 8              '//eBool	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_WRITE_ORDER As Integer = 9              '//eBool	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_WRITE_INVOICE As Integer = 10              '//eBool	
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_WRITE_DELIVERY_NOTE As Integer = 11              '//eBool
        Public Const ADK_CODE_OF_ARTICLE_PARCEL_LAST As Integer = 11


        Public Const ADK_CODE_OF_ARTICLE_NAME_FIRST As Integer = 0
        Public Const ADK_CODE_OF_ARTICLE_NAME_LANGUAGE_CODE As Integer = 0              '//eChar
        Public Const ADK_CODE_OF_ARTICLE_NAME_ARTICLE_NUMBER As Integer = 1              '//eChar
        Public Const ADK_CODE_OF_ARTICLE_NAME_NAME As Integer = 2              '//eChar
        Public Const ADK_CODE_OF_ARTICLE_NAME_NAME2 As Integer = 3              '//eChar
        Public Const ADK_CODE_OF_ARTICLE_NAME_LAST As Integer = 3


        Public Const ADK_PRICE_FIRST As Integer = 0
        Public Const ADK_PRICE_PRICE_LIST As Integer = 0              '//eChar	
        Public Const ADK_PRICE_ARTICLE_NUMBER As Integer = 1              '//eChar	
        Public Const ADK_PRICE_QUANTITY As Integer = 2              '//eDouble
        Public Const ADK_PRICE_PRICE As Integer = 3              '//eDouble
        Public Const ADK_PRICE_BASE As Integer = 4              '//eBool	
        Public Const ADK_PRICE_PER_CENT As Integer = 5              '//eDouble
        Public Const ADK_PRICE_DATE_OF_CHANGE As Integer = 6              '//eDate
        Public Const ADK_PRICE_LAST As Integer = 6


        Public Const ADK_ARTICLE_PURCHASE_PRICE_FIRST As Integer = 0
        Public Const ADK_ARTICLE_PURCHASE_PRICE_ARTICLE_NUMBER As Integer = 0              '//eChar	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_SUPPLIER_NUMBER As Integer = 1              '//eChar	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_QUANTITY As Integer = 2              '//eDouble
        Public Const ADK_ARTICLE_PURCHASE_PRICE_PRICE As Integer = 3              '//eDouble
        Public Const ADK_ARTICLE_PURCHASE_PRICE_SUPPLIER_ARTICLE_NUMBER As Integer = 4              '//eChar	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_BASE As Integer = 5              '//eBool	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_CHANGED As Integer = 6              '//eBool	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_CHANGED_DATE As Integer = 7              '//eDate	
        Public Const ADK_ARTICLE_PURCHASE_PRICE_PRICE_NEW As Integer = 8              '//eDouble
        Public Const ADK_ARTICLE_PURCHASE_PRICE_PRICE_PER_CENT As Integer = 9              '//eDouble
        Public Const ADK_ARTICLE_PURCHASE_PRICE_LAST As Integer = 9


        Public Const ADK_CODE_OF_WAY_OF_PAYMENT_FIRST As Integer = 0
        Public Const ADK_CODE_OF_WAY_OF_PAYMENT_CODE As Integer = 0              '//eChar
        Public Const ADK_CODE_OF_WAY_OF_PAYMENT_TEXT As Integer = 1              '//eChar
        Public Const ADK_CODE_OF_WAY_OF_PAYMENT_ACCOUNT As Integer = 2              '//eChar
        Public Const ADK_CODE_OF_WAY_OF_PAYMENT_LAST As Integer = 2


        '//SPCS Förening
        Public Const ADK_FREE_CATEGORY_FIRST As Integer = 0
        Public Const ADK_FREE_CATEGORY_CATEGORY As Integer = 0
        Public Const ADK_FREE_CATEGORY_NAME As Integer = 1
        Public Const ADK_FREE_CATEGORY_LAST As Integer = 1


        '//SPCS Förening
        Public Const ADK_MEMBER_FIRST As Integer = 0
        Public Const ADK_MEMBER_NUMBER As Integer = 0              '//eChar 
        Public Const ADK_MEMBER_FIRST_NAME As Integer = 1              '//eChar 
        Public Const ADK_MEMBER_LAST_NAME As Integer = 2              '//eChar 
        Public Const ADK_MEMBER_OWN_ADDRESS As Integer = 3              '//eChar 
        Public Const ADK_MEMBER_MAILING_ADDRESS As Integer = 4              '//eBool 
        Public Const ADK_MEMBER_MAILING_ADDRESS2 As Integer = 5              '//eChar
        Public Const ADK_MEMBER_ZIPCODE As Integer = 6              '//eChar 
        Public Const ADK_MEMBER_CITY As Integer = 7              '//eChar 
        Public Const ADK_MEMBER_COUNTRY As Integer = 8              '//eChar 
        Public Const ADK_MEMBER_TELEPHONE As Integer = 9              '//eChar 
        Public Const ADK_MEMBER_TELEPHONE2 As Integer = 10              '//eChar 
        Public Const ADK_MEMBER_PERSONAL_NUMBER As Integer = 11              '//eChar 
        Public Const ADK_MEMBER_SEX As Integer = 12              '//eDouble
        Public Const ADK_MEMBER_EMAIL As Integer = 13              '//eChar 
        Public Const ADK_MEMBER_DOCUMENT_PATH As Integer = 14              '//eChar 
        Public Const ADK_MEMBER_REMARK1 As Integer = 15              '//eChar 
        Public Const ADK_MEMBER_REMARK2 As Integer = 16              '//eChar 
        Public Const ADK_MEMBER_CATEGORY As Integer = 17              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY1 As Integer = 18              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY2 As Integer = 19              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY3 As Integer = 20              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY4 As Integer = 21              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY5 As Integer = 22              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY6 As Integer = 23              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY7 As Integer = 24              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY8 As Integer = 25              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY9 As Integer = 26              '//eChar 
        Public Const ADK_MEMBER_FREE_CATEGORY10 As Integer = 27              '//eChar 
        Public Const ADK_MEMBER_SORT_ORDER As Integer = 28              '//eChar 
        Public Const ADK_MEMBER_BGIRO As Integer = 29              '//eChar 
        Public Const ADK_MEMBER_PGIRO As Integer = 30              '//eChar 
        Public Const ADK_MEMBER_CODE_OF_TERMS_OF_PAYMENT As Integer = 31              '//eChar 
        Public Const ADK_MEMBER_PRICE_LIST As Integer = 32              '//eChar 
        Public Const ADK_MEMBER_INTEREST_INVOICE As Integer = 33              '//eBool 
        Public Const ADK_MEMBER_REMINDER As Integer = 34              '//eBool 
        Public Const ADK_MEMBER_DEMAND_FEE As Integer = 35              '//eBool 
        Public Const ADK_MEMBER_FAMILY_NUMBER As Integer = 36              '//eChar 
        Public Const ADK_MEMBER_LAST As Integer = 36


        Public Const ADK_DELIVERY_NOTE_FIRST As Integer = 0
        Public Const ADK_DELIVERY_NOTE_DOCUMENT_NUMBER As Integer = 0               '//eDouble
        Public Const ADK_DELIVERY_NOTE_SUPPLIER_NUMBER As Integer = 1               '//eChar	
        Public Const ADK_DELIVERY_NOTE_SUPPLIER_NAME As Integer = 2               '//eChar	
        Public Const ADK_DELIVERY_NOTE_DELIVERY_NUMBER As Integer = 3               '//eChar	
        Public Const ADK_DELIVERY_NOTE_DATE As Integer = 4               '//eDate	
        Public Const ADK_DELIVERY_NOTE_ARRIVAL_DATE As Integer = 5               '//eDate	
        Public Const ADK_DELIVERY_NOTE_CANCELLED As Integer = 6               '//eBool	
        Public Const ADK_DELIVERY_NOTE_NOTE1 As Integer = 7               '//eChar	
        Public Const ADK_DELIVERY_NOTE_NOTE2 As Integer = 8               '//eChar	
        Public Const ADK_DELIVERY_NOTE_NOTE3 As Integer = 9               '//eChar	
        Public Const ADK_DELIVERY_NOTE_PROJECT_CODE As Integer = 10              '//eChar	
        Public Const ADK_DELIVERY_NOTE_PROFIT_CENTRE As Integer = 11              '//eChar	
        Public Const ADK_DELIVERY_NOTE_INVOICE_SENT As Integer = 12              '//eBool	
        Public Const ADK_DELIVERY_NOTE_CURRENCY_CODE As Integer = 13              '//eChar
        Public Const ADK_DELIVERY_NOTE_ROWS As Integer = 14              '//eData   
        Public Const ADK_DELIVERY_NOTE_NROWS As Integer = 15              '//eDouble 
        Public Const ADK_DELIVERY_NOTE_DN_ARRIVAL_DATE As Integer = 16              '//eDate	
        Public Const ADK_DELIVERY_NOTE_DN_DELIVER_DATE As Integer = 17              '//eDate	
        Public Const ADK_DELIVERY_NOTE_DN_NUMBER As Integer = 18              '//eChar	
        Public Const ADK_DELIVERY_NOTE_DN_NOT_DONE As Integer = 19              '//eBool   
        Public Const ADK_DELIVERY_NOTE_DN_CREATED_EDI As Integer = 20              '//eBool	
        Public Const ADK_DELIVERY_NOTE_LAST As Integer = 20


        Public Const ADK_PACKAGE_HEAD_FIRST As Integer = 0
        Public Const ADK_PACKAGE_HEAD_PACKAGE_NUMBER As Integer = 0               '//eChar	
        Public Const ADK_PACKAGE_HEAD_PACKAGE_REFERENCE As Integer = 1               '//eChar	
        Public Const ADK_PACKAGE_HEAD_QUANTITY As Integer = 2               '//eDouble	
        Public Const ADK_PACKAGE_HEAD_TYPE As Integer = 3               '//eDate	
        Public Const ADK_PACKAGE_HEAD_GROSS_WEIGHT As Integer = 4               '//eDouble	
        Public Const ADK_PACKAGE_HEAD_GROSS_VOLUME As Integer = 5               '//eDouble	
        Public Const ADK_PACKAGE_HEAD_NET_WEIGHT As Integer = 6               '//eDouble	
        Public Const ADK_PACKAGE_HEAD_NET_VOLUME As Integer = 7               '//eDouble	
        Public Const ADK_PACKAGE_HEAD_AREA As Integer = 8               '//eChar	
        Public Const ADK_PACKAGE_HEAD_IDENT As Integer = 9               '//eChar	
        Public Const ADK_PACKAGE_HEAD_BATCH_NUMBER As Integer = 10              '//eChar	
        Public Const ADK_PACKAGE_HEAD_CONNECTION_DOCUMENT As Integer = 11              '//eDouble
        Public Const ADK_PACKAGE_HEAD_BEST_BEFORE As Integer = 12              '//eDate
        Public Const ADK_PACKAGE_HEAD_ROWS As Integer = 13              '//eData
        Public Const ADK_PACKAGE_HEAD_NROWS As Integer = 14              '//eDouble    
        Public Const ADK_PACKAGE_HEAD_LAST As Integer = 14


        Public Const ADK_PACKAGE_ROW_FIRST As Integer = 0
        Public Const ADK_PACKAGE_ROW_ROWNR As Integer = 0                '//eChar
        Public Const ADK_PACKAGE_ROW_NAME As Integer = 1               '//eChar
        Public Const ADK_PACKAGE_ROW_QUANTITY As Integer = 2               '//eDouble	
        Public Const ADK_PACKAGE_ROW_ARTICLE_NUMBER As Integer = 3               '//eChar	
        Public Const ADK_PACKAGE_ROW_SERIAL_NUMBER As Integer = 4               '//eChar	
        Public Const ADK_PACKAGE_ROW_BATCH_NUMBER As Integer = 5               '//eChar	
        Public Const ADK_PACKAGE_ROW_BEST_BEFORE As Integer = 6               '//eDate	
        Public Const ADK_PACKAGE_ROW_ORDER_ROW_NUMBER As Integer = 7               '//eDouble	
        Public Const ADK_PACKAGE_ROW_LAST As Integer = 7


        Public Const ADK_IMP_PACKAGE_HEAD_FIRST As Integer = 0
        Public Const ADK_IMP_PACKAGE_HEAD_PACKAGE_NUMBER As Integer = 0               '//eChar	
        Public Const ADK_IMP_PACKAGE_HEAD_QUANTITY As Integer = 1               '//eDouble	
        Public Const ADK_IMP_PACKAGE_HEAD_TYPE As Integer = 2               '//eDate	
        Public Const ADK_IMP_PACKAGE_HEAD_GROSS_WEIGHT As Integer = 3               '//eDouble	
        Public Const ADK_IMP_PACKAGE_HEAD_GROSS_VOLUME As Integer = 4               '//eDouble	
        Public Const ADK_IMP_PACKAGE_HEAD_NET_WEIGHT As Integer = 5               '//eDouble	
        Public Const ADK_IMP_PACKAGE_HEAD_NET_VOLUME As Integer = 6               '//eDouble	
        Public Const ADK_IMP_PACKAGE_HEAD_AREA As Integer = 7               '//eChar	
        Public Const ADK_IMP_PACKAGE_HEAD_IDENT As Integer = 8               '//eChar	
        Public Const ADK_IMP_PACKAGE_HEAD_BATCH_NUMBER As Integer = 9               '//eChar	
        Public Const ADK_IMP_PACKAGE_HEAD_CONNECTION_DOCUMENT As Integer = 10              '//eDouble
        Public Const ADK_IMP_PACKAGE_HEAD_BEST_BEFORE As Integer = 11              '//eDate
        Public Const ADK_IMP_PACKAGE_HEAD_ROWS As Integer = 12              '//eData
        Public Const ADK_IMP_PACKAGE_HEAD_NROWS As Integer = 13              '//eDouble    
        Public Const ADK_IMP_PACKAGE_HEAD_LAST As Integer = 13


        Public Const ADK_IMP_PACKAGE_ROW_FIRST As Integer = 0
        Public Const ADK_IMP_PACKAGE_ROW_ROWNR As Integer = 0               '//eChar
        Public Const ADK_IMP_PACKAGE_ROW_NAME As Integer = 1               '//eChar
        Public Const ADK_IMP_PACKAGE_ROW_QUANTITY As Integer = 2               '//eDouble	
        Public Const ADK_IMP_PACKAGE_ROW_ARTICLE_NUMBER As Integer = 3               '//eChar	
        Public Const ADK_IMP_PACKAGE_ROW_SERIAL_NUMBER As Integer = 4               '//eChar	
        Public Const ADK_IMP_PACKAGE_ROW_BATCH_NUMBER As Integer = 5               '//eChar	
        Public Const ADK_IMP_PACKAGE_ROW_BEST_BEFORE As Integer = 6               '//eDate	
        Public Const ADK_IMP_PACKAGE_ROW_LAST As Integer = 6

        Public Const ADKE_OK As Integer = 0
        Public Const ADKE_INTERNAL As Integer = 1
        Public Const ADKE_CUST_DISCOUNT_EXIST As Integer = 2
        Public Const ADKE_EXPORT_INVOICE As Integer = 3
        Public Const ADKE_OOF_EDIT_DENIED As Integer = 4
        Public Const ADKE_NOT_PRINTED_IF_NOT_READY As Integer = 5
        Public Const ADKE_NO_CHANGE_ON_EXPORT As Integer = 6
        Public Const ADKE_NO_ARTNR_ON_TEXT_ROW As Integer = 7
        Public Const ADKE_NO_ARTNR_CHANGE_ON_PACKET As Integer = 8
        Public Const ADKE_EDIT_DENIED_OBLITERATED As Integer = 9
        Public Const ADKE_OFFER_EDIT_DENIED_CREATED As Integer = 10
        Public Const ADKE_ORDER_EDIT_DENIED_CREATED As Integer = 11
        Public Const ADKE_EDIT_DENIED_PRINTED As Integer = 12
        Public Const ADKE_ORDER_EDIT_DENIED_DELIVERED As Integer = 13
        Public Const ADKE_ORDER_EDIT_DENIED_NEW_ORDER_CREATED As Integer = 14
        Public Const ADKE_ORDER_EDIT_DENIED_NO_GROUP_INVOICE As Integer = 15
        Public Const ADKE_ORDER_EDIT_DENIED_ALREADY_REST As Integer = 16
        Public Const ADKE_ORDER_EDIT_DENIED_PACKET_NO_REST As Integer = 17
        Public Const ADKE_OOF_EDIT_DENIED_SUBSUM As Integer = 18
        Public Const ADKE_OOF_EDIT_DENIED_ALREADY_IN_LEDGER As Integer = 19
        Public Const ADKE_COULD_NOT_FIND_ARTICLE As Integer = 20
        Public Const ADKE_COULD_NOT_FIND_CUSTOMER As Integer = 21
        Public Const ADKE_DENIED_PROFIT_CENTRE_ON_ACCOUNT As Integer = 22
        Public Const ADKE_INVOICE_EDIT_DENIED_JOURNAL As Integer = 23
        Public Const ADKE_INVOICE_EDIT_DENIED_BOOKKEEPING As Integer = 24
        Public Const ADKE_INVOICE_EDIT_DENIED_ALREADY_IN_LEDGER As Integer = 25
        Public Const ADKE_INVOICE_EDIT_DENIED_OTHER_INVOICE As Integer = 26
        Public Const ADKE_INVOICE_EDIT_DENIED_INTREST_LINE As Integer = 27
        Public Const ADKE_INVOICE_EDIT_DENIED_CUSTOMER_NR As Integer = 28
        Public Const ADKE_INVOICE_EDIT_DENIED_ORDERINFO As Integer = 29
        Public Const ADKE_INVOICE_EDIT_DENIED_AGREEMENT_PERIOD As Integer = 30
        Public Const ADKE_INVOICE_EDIT_DENIED_MANUAL As Integer = 31
        Public Const ADKE_ARTICLE_DENIED_KLKPRICE_MANUAL As Integer = 32
        Public Const ADKE_ARTICLE_DENIED_PARCELART_STOCK As Integer = 33
        Public Const ADKE_INVALID_STRING As Integer = 34
        Public Const ADKE_INVALID_PRISL As Integer = 35
        Public Const ADKE_ORDER_EDIT_DENIED_OFFER_CANT_BE_LOCKED As Integer = 36
        Public Const ADKE_CUSTOMER_INVALID_KUNDKAT As Integer = 37
        Public Const ADKE_CUSTOMER_INVALID_DISTRIKT As Integer = 38
        Public Const ADKE_CUSTOMER_INVALID_SELLER As Integer = 39
        Public Const ADKE_INVALID_DELIVERY_TERMS As Integer = 40
        Public Const ADKE_INVALID_PAYMENT_TERMS As Integer = 41
        Public Const ADKE_INVALID_DELIVERY_WAY As Integer = 42
        Public Const ADKE_INVALID_CURRENCY_CODE As Integer = 43
        Public Const ADKE_INVALID_LAUNGUAGE_CODE As Integer = 44
        Public Const ADKE_ARTICLE_INVALID_ARTGRP As Integer = 45
        Public Const ADKE_ARTICLE_INVALID_KONTKOD As Integer = 46
        Public Const ADKE_ARTICLE_INVALID_UNITCODE As Integer = 47
        Public Const ADKE_ARTICLE_INVALID_SUPPLIER As Integer = 48
        Public Const ADKE_OOF_INVALID_CUSTOMER As Integer = 49
        Public Const ADKE_OOF_INVALID_ARTICLE As Integer = 50
        Public Const ADKE_CUSTOMER_INVALID_CUSTOMERNR As Integer = 51

        'databastabeller;
        Public Const ADKE_DBTABLE_CUSTOMER As Integer = 52
        Public Const ADKE_DBTABLE_ARTICLE As Integer = 53
        Public Const ADKE_DBTABLE_OOF As Integer = 54
        Public Const ADKE_DBTABLE_ARTRAD As Integer = 55
        Public Const ADKE_DBTABLE_KUNDKAT As Integer = 56
        Public Const ADKE_DBTABLE_DISTRICT As Integer = 57
        Public Const ADKE_DBTABLE_SELLER As Integer = 58
        Public Const ADKE_DBTABLE_DEL_TERMS As Integer = 59
        Public Const ADKE_DBTABLE_PAYMENT_TERMS As Integer = 60
        Public Const ADKE_DBTABLE_CURRENCY_CODE As Integer = 61
        Public Const ADKE_DBTABLE_LANGUAGE_CODE As Integer = 62
        Public Const ADKE_DBTABLE_PRICELIST As Integer = 63
        Public Const ADKE_DBTABLE_ARTGROUP As Integer = 64
        Public Const ADKE_DBTABLE_ARTKNT_KONTKOD As Integer = 65
        Public Const ADKE_DBTABLE_UNIT_CODE As Integer = 66
        Public Const ADKE_DBTABLE_SUPPLIER As Integer = 67
        Public Const ADKE_DBTABLE_PRICEL As Integer = 68
        Public Const ADKE_DBTABLE_LEVFKT As Integer = 69
        Public Const ADKE_DBTABLE_ACCOUNT As Integer = 70
        Public Const ADKE_DBTABLE_PROJECT As Integer = 71
        Public Const ADKE_DBTABLE_DEL_WAY As Integer = 72
        Public Const ADKE_DBTABLE_DISCOUNT_AGREEMENT As Integer = 73
        Public Const ADKE_DBTABLE_RESULT_UNIT As Integer = 74

        Public Const ADKE_INVALID_DB_PROG As Integer = 75
        Public Const ADKE_INVALID_DB_NEWER As Integer = 76
        Public Const ADKE_INVALID_DB_OLDER As Integer = 77
        Public Const ADKE_BOF As Integer = 78
        Public Const ADKE_EOF As Integer = 79
        Public Const ADKE_NO_DB_OPEN As Integer = 80

        'Funktioner;
        Public Const ADKE_ADD As Integer = 81
        Public Const ADKE_FIND As Integer = 82
        Public Const ADKE_FIRST As Integer = 83
        Public Const ADKE_NEXT As Integer = 84
        Public Const ADKE_UPDATE As Integer = 85
        Public Const ADKE_SET_Integer As Integer = 86
        Public Const ADKE_SET_BOOL As Integer = 87
        Public Const ADKE_SET_DOUBLE As Integer = 88
        Public Const ADKE_SET_STRING As Integer = 89
        Public Const ADKE_SET_DATA As Integer = 90
        Public Const ADKE_SET_DATE As Integer = 91
        Public Const ADKE_GET_STRING As Integer = 92
        Public Const ADKE_GET_Integer As Integer = 93
        Public Const ADKE_GET_BOOL As Integer = 94
        Public Const ADKE_GET_DOUBLE As Integer = 95
        Public Const ADKE_GET_DATE As Integer = 96
        Public Const ADKE_GET_TYPE As Integer = 97
        Public Const ADKE_GET_DATA As Integer = 98
        Public Const ADKE_GET_DATA_ROW As Integer = 99
        Public Const ADKE_OPEN As Integer = 100

        Public Const ADKE_DELETE_STRUCT As Integer = 101
        Public Const ADKE_UNKNON_DB_FIELD As Integer = 102
        Public Const ADKE_INVALID_DATA_TYPE As Integer = 103
        Public Const ADKE_NOT_VALID_SORTORDER As Integer = 104
        Public Const ADKE_NO_PRICELIST As Integer = 105
        Public Const ADKE_COULD_NOT_FIND_SUPPLIER As Integer = 106
        Public Const ADKE_INVALID_DB As Integer = 107
        Public Const ADKE_INVALID_CONTROLNR As Integer = 108
        Public Const ADKE_EDIT_DENIED_FIELD As Integer = 109
        Public Const ADKE_INVALID_INVOICE_TYPE As Integer = 110
        Public Const ADKE_INVALID_INDEX As Integer = 111
        Public Const ADKE_NO_KEY As Integer = 112
        Public Const ADKE_Integer_TO_DATE As Integer = 113
        Public Const ADKE_TO_SHORT_STRING As Integer = 114
        Public Const ADKE_NOT_FOUND As Integer = 115
        Public Const ADKE_INVALID_PATH_COMPANY As Integer = 116
        Public Const ADKE_SET_SORT_ORDER As Integer = 117
        Public Const ADKE_NO_ACTIVE_PRICE_LIST As Integer = 118
        Public Const ADKE_RECIEVER_NUMBER_USED As Integer = 119
        Public Const ADKE_INVALID_CODE_OF_FEE As Integer = 120
        Public Const ADKE_DENIED_DELETE_CODE_SWE As Integer = 121
        Public Const ADKE_DENIED_DELETE_CODE_DOMESTIC As Integer = 122
        Public Const ADKE_DENIED_DELETE_CODE_USED As Integer = 123
        Public Const ADKE_IN_DISCOUNT_AGREEMENT As Integer = 124
        Public Const ADKE_DENIED_DELETE_RES_CODE_USED As Integer = 125
        Public Const ADKE_DENIED_DELETE_RES_CODE_ING As Integer = 126
        Public Const ADKE_DENIED_DELETE_RES_CODE_BUDGET As Integer = 127
        Public Const ADKE_INVALID_NET_LICENCE As Integer = 128
        Public Const ADKE_INVALID_TIME As Integer = 129
        Public Const ADKE_INVALID_SERIAL As Integer = 130
        Public Const ADKE_INVALID_ADM_PATH_INI As Integer = 131
        Public Const ADKE_APP_OLD As Integer = 132
        Public Const ADKE_APP_NEW As Integer = 133
        Public Const ADKE_CANT_OPEN_FILE As Integer = 134
        Public Const ADKE_TOO_MANY_LICENCE As Integer = 135
        Public Const ADKE_UTDATE_ARTICLE_NUMBER_DENIED As Integer = 136
        Public Const ADKE_INVALID_ADM_PATH As Integer = 137
        Public Const ADKE_DATE_TO_Integer As Integer = 138
        Public Const ADKE_NO_NAME As Integer = 139
        Public Const ADKE_EDIT_DENIED_PAYMENT_JOURNAL As Integer = 140
        Public Const ADKE_EDIT_DENIED_INVOICE_PAYED As Integer = 141
        Public Const ADKE_EDIT_DENIED_PAYMENTS_ON_INVOICE As Integer = 142
        Public Const ADKE_EDIT_DENIED_ROWS_TO_SUPPLIER As Integer = 143
        Public Const ADKE_DENIED_BAD_ROW_TYPE As Integer = 144
        Public Const ADKE_DENIED_INVOICE_ROW As Integer = 145
        Public Const ADKE_DENIED_BST_OTHER_SUPPLIER As Integer = 146
        Public Const ADKE_DENIED_COLLECTION_INVOICE As Integer = 147
        Public Const ADKE_INVLAID_ACCOUNT As Integer = 148
        Public Const ADKE_INVLAID_PROJECT As Integer = 149
        Public Const ADKE_INVLAID_RESULT_UNIT As Integer = 150
        Public Const ADKE_NO_ARTICLE_TO_DELIVER As Integer = 151
        Public Const ADKE_INVALID_DISCOUNT_AGGREMENT As Integer = 152
        Public Const ADKE_INVALID_ROW_CONNECTION As Integer = 153
        Public Const ADKE_DELETE As Integer = 154
        Public Const ADKE_DELETE_DENIED_CONNECTION As Integer = 155
        Public Const ADKE_DELETE_DENIED_DEBT As Integer = 156
        Public Const ADKE_DELETE_DENIED_STOCK As Integer = 157
        Public Const ADKE_DELETE_DENIED_RESERVATION As Integer = 158
        Public Const ADKE_DELETE_DENIED_ORDER As Integer = 159
        Public Const ADKE_DELETE_DENIED_PARCEL As Integer = 160
        Public Const ADKE_DELETE_DENIED_INVALID_TYPE As Integer = 161
        Public Const ADKE_DELETE_DENIED_INVOICE_ROWS As Integer = 162
        Public Const ADKE_DELETE_DENIED_ONLY_PLANED_DIRECT As Integer = 163
        Public Const ADKE_DELETE_DENIED As Integer = 164
        Public Const ADKE_DELETE_ROW As Integer = 165
        Public Const ADKE_INVALID_BST As Integer = 166
        Public Const ADKE_END_BEFORE_START As Integer = 167
        Public Const ADKE_END_AND_START As Integer = 168
        Public Const ADKE_INVALID_FLJ As Integer = 169
        Public Const ADKE_NOT_TYPE_AND_DOC As Integer = 170
        Public Const ADKE_INVALID_OFFER As Integer = 171
        Public Const ADKE_INVALID_ORDER As Integer = 172
        Public Const ADKE_INVALID_INVOICE As Integer = 173
        Public Const ADKE_INVALID_AGREEMENT As Integer = 174
        Public Const ADKE_INVALID_JOURNAL As Integer = 175
        Public Const ADKE_INVALID_VER As Integer = 176
        Public Const ADKE_NO_SEARCH_VALUE As Integer = 177
        Public Const ADKE_GET_LENGTH As Integer = 178
        Public Const ADKE_GET_DECIMALS As Integer = 179
        Public Const ADKE_TOO_MANY_SIGNS As Integer = 180
        Public Const ADKE_TOO_MANY_DECIMALS As Integer = 181
        Public Const ADKE_EDIT_DENIED_ON_TEXT_ROW As Integer = 182
        Public Const ADKE_CANT_FIND_ACCOUNT As Integer = 183
        Public Const ADKE_OLD_PROJECT_NUMBER As Integer = 184
        Public Const ADKE_DB_NOT_ADM_2000 As Integer = 185
        Public Const ADKE_GET_FIELD_NAME As Integer = 186

        Public Const ADKE_NO_DB_INVOLVED As Integer = 187
        Public Const ADKE_NO_FIELD_INVOLVED As Integer = 188

        Public Const ADKE_QUARTER_DENIED_IF_NO_VAT As Integer = 189
        Public Const ADKE_DENIED_FOLJ_MAKUL As Integer = 190
        Public Const ADKE_DENIED_FOLJ_INVALID_SUPPIER As Integer = 191
        Public Const ADKE_DENIED_PROJECT_ON_ACCOUNT As Integer = 192
        Public Const ADKE_DENIED_DELETE_CURRENCY_CODE_SWE As Integer = 193
        Public Const ADKE_DENIED_DELETE_CURRENCY_CODE_DOMESTIC As Integer = 194
        Public Const ADKE_CAN_NOT_CHANGE_ARTICLE_ROW_TO_TEXT_ROW As Integer = 195
        Public Const ADKE_CAN_NOT_CHANGE_TEXT_ROW_TO_ARTICLE_ROW As Integer = 196
        Public Const ADKE_INVALID_ROW_TYPE As Integer = 197
        Public Const ADKE_CAN_NOT_CHANGE_TEXT_ROW_TO_SUB_SUM As Integer = 198
        Public Const ADKE_CAN_NOT_CHANGE_OOF_TYPE As Integer = 199
        Public Const ADKE_CAN_NOT_CHANGE_SUB_SUM_TO_TEXT_ROW As Integer = 200
        Public Const ADKE_DENIED_UPDATE_TEXT_ROW As Integer = 201
        Public Const ADKE_ARTICLE_DENIED_NEED_GROUND_PRICE As Integer = 202
        Public Const ADKE_ORDER_EDIT_DENIED_CHANGE_DATE_ON_PACKET As Integer = 203
        Public Const ADKE_PREVIOUS As Integer = 204
        Public Const ADKE_LAST As Integer = 205
        Public Const ADKE_CANT_UPDATE_ART_NR As Integer = 206
        Public Const ADKE_DB_NOT_ADM_1000 As Integer = 207
        Public Const ADKE_FIELD_NOT_IN_ADM_VER As Integer = 208
        Public Const ADKE_CURRENCY_LANGUAGE_INACTIVE As Integer = 209
        Public Const ADKE_BARCODE_INACTIVE As Integer = 210
        Public Const ADKE_INVALID_STRUCT_POINTER As Integer = 211
        Public Const ADKE_INVALID_STRUCT_POINTER_TO_ROW As Integer = 212
        Public Const ADKE_ARTICLE_INVALID_BARCODE As Integer = 213
        Public Const ADKE_KEY_TAKEN_IN_NUMBERSERIES As Integer = 214
        Public Const ADKE_INVALID_SUPPLIER_ART_NUMBER As Integer = 215
        Public Const ADKE_INVALID_POST As Integer = 216
        Public Const ADKE_ORDER_HAS_CREATED_INVOICE As Integer = 217
        Public Const ADKE_ORDER_HAS_OTHER_OFFERNR As Integer = 218
        Public Const ADKE_OFFER_HAS_OTHER_ORDERNR As Integer = 219
        Public Const ADKE_ORDER_HAS_OTHER_INVOICENR As Integer = 220
        Public Const ADKE_INVOICE_HAS_OTHER_ORDERNR As Integer = 221
        Public Const ADKE_OOF_INVALID_BARCODE As Integer = 222
        Public Const ADKE_API_APP_DIFF As Integer = 223
        Public Const ADKE_EDIT_DENIED_ALREADY_IN_LEDGER As Integer = 224
        Public Const ADKE_DELIVERY_NO_STOCK_OBJECT As Integer = 225
        Public Const ADKE_QUANTITY_0 As Integer = 226
        Public Const ADKE_NO_ARTNR As Integer = 227
        Public Const ADKE_ONLY_OBLITERATED As Integer = 228
        Public Const ADKE_DBTABLE_INVENTORY As Integer = 229
        Public Const ADKE_DBTABLE_MANUAL_DELIVERY_IN As Integer = 230
        Public Const ADKE_DBTABLE_MANUAL_DELIVERY_OUT As Integer = 231
        Public Const ADKE_DBTABLE_DISPATCHER As Integer = 232
        Public Const ADKE_DENIED_CREDIT_CREDIT_INVOICE As Integer = 233
        Public Const ADKE_DENIED_CREDIT_OBLITERATED_INVOICE As Integer = 234
        Public Const ADKE_INVALID_TYPE_TO_COPY As Integer = 235
        Public Const ADKE_INVALID_DISPATCHER As Integer = 236
        Public Const ADKE_EDIT_DENIED_ON_THIS_INVOICE As Integer = 237

        'Warnings
        Public Const ADKW_CREDIT_LIMIT As Integer = 238
        Public Const ADKW_RECIEVER_NUMBER_UNIQUE As Integer = 239
        Public Const ADKW_ONLY_NUMBERS_IN_SWIFT_ADDRES As Integer = 240
        Public Const ADKW_CHANGE_CURRENCY_CODE As Integer = 241
        Public Const ADKW_OCR_FOREIGN As Integer = 242
        Public Const ADKW_OCR_SUPPLIER As Integer = 243
        Public Const ADKW_OCR_ZIPCODE As Integer = 244
        Public Const ADKW_DENIED_FOLJESEDEL As Integer = 245
        Public Const ADKW_ARTICLE_STOCK_TRANSACTION As Integer = 246
        Public Const ADKW_ORDER_SUM As Integer = 247
        Public Const ADKW_VER_ON_PROJECT As Integer = 248
        Public Const ADKW_NOT_OCR_BUT_ZIP As Integer = 249
        Public Const ADKW_OCR_BUT_NOT_ZIP As Integer = 250
        Public Const ADKW_COULD_NOT_FIND_ARTICLE_PRICE As Integer = 251
        Public Const ADKW_CHANGED_STOCK As Integer = 252
        Public Const ADKW_CANT_FIND_TEMPLATE As Integer = 253
        Public Const ADKW_INVALID_FIND_TEMPLATE As Integer = 254
        Public Const ADKW_UPDATING_TEXTROW As Integer = 255
        Public Const ADKW_ACCOUNT_NOT_ACTIVE As Integer = 256
        Public Const ADKW_CALCPRICE_MISSING As Integer = 257
        Public Const ADKW_DELIVERY_IN_NO_PRICE As Integer = 258
        Public Const ADKW_ROWS_NOT_UPDATED As Integer = 259
        Public Const ADKW_SWIFT_AND_BG_NECESSARY As Integer = 260 '//Används inte längre (Fr o m ver 4.1)

        Public Const ADKE_CAN_NOT_CONNECT_CREDIT As Integer = 261
        Public Const ADKE_ORDER_DOESNT_EXIST As Integer = 262
        Public Const ADKE_ORDER_MAKUL As Integer = 263
        Public Const ADKE_OFFER_DOESNT_EXIST As Integer = 264
        Public Const ADKE_OFFER_MAKUL As Integer = 265
        Public Const ADKE_OOI_CONNECT_NROWS_NOT_ZERO As Integer = 266
        Public Const ADKE_CONNECTION_NUMBER1 As Integer = 267
        Public Const ADKE_CANT_ERASE_BG_SWIFT As Integer = 268
        Public Const ADKE_ONLY_FOR_EUR_SEK As Integer = 269
        Public Const ADKE_BG_CODE_OF_FEE_FOR_PAYMENT_ABROAD As Integer = 270
        Public Const ADKE_BG_CODE_OF_FEE_MUST_BE_RECIP_PAYS_FOREIGN As Integer = 271
        Public Const ADKE_BG_CODE_OF_FEE_NOT_SUPPORTED_BY_BANK As Integer = 272
        Public Const ADKE_NO_CHANGE_CLOSED_PROJECT1 As Integer = 273

        Public Const ADKE_DB_NOT_ADM_500 As Integer = 274
        Public Const ADKE_DB_NOT_ADM_FOR As Integer = 275
        Public Const ADKE_DBTABLE_BOOKING As Integer = 276
        Public Const ADKE_INVOICE_OR_DELIVERY_NOTE_PRINTED As Integer = 277
        Public Const ADKE_DBTABLE_NOT_IN_ADM_VER As Integer = 278
        Public Const ADKE_INVALID_COUNTRY_CODE As Integer = 279
        Public Const ADKE_DBTABLE_PRICE As Integer = 280

        Public Const ADKE_GETADMSIZE As Integer = 281
        Public Const ADKE_CANT_CHANGE_WRITE_FLAG As Integer = 282
        Public Const ADKE_CANT_CHANGE_WHEN_WRITE_FLAG As Integer = 283
        Public Const ADKE_DOC_SENT_EDI As Integer = 284

        Public Const ADKE_DBTABLE_MEMBER As Integer = 285
        Public Const ADKE_DELETE_DENIED_HEAD_OF_FAMILY As Integer = 286
        Public Const ADKE_DELETE_DENIED_MEMBER_OF_FAMILY As Integer = 287
        Public Const ADKE_HEAD_OF_FAMILY_NEED_OWN_ADRESS As Integer = 288

        Public Const ADKW_FAILED_LOAD_ADDRESS_FROM_HEAD_OF_FAMILY As Integer = 289
        Public Const ADKW_FAILED_LOAD_ADDRESS_TO_MEMBER_OF_FAMILY As Integer = 290

        Public Const ADKE_INVALID_FREE_CATEGORY_1 As Integer = 291
        Public Const ADKE_INVALID_FREE_CATEGORY_2 As Integer = 292
        Public Const ADKE_INVALID_FREE_CATEGORY_3 As Integer = 293
        Public Const ADKE_INVALID_FREE_CATEGORY_4 As Integer = 294
        Public Const ADKE_INVALID_FREE_CATEGORY_5 As Integer = 295
        Public Const ADKE_INVALID_FREE_CATEGORY_6 As Integer = 296
        Public Const ADKE_INVALID_FREE_CATEGORY_7 As Integer = 297
        Public Const ADKE_INVALID_FREE_CATEGORY_8 As Integer = 298
        Public Const ADKE_INVALID_FREE_CATEGORY_9 As Integer = 299
        Public Const ADKE_INVALID_FREE_CATEGORY_10 As Integer = 300
        Public Const ADKE_FREE_CATEGORY_1_INACTIVE As Integer = 301
        Public Const ADKE_FREE_CATEGORY_2_INACTIVE As Integer = 302
        Public Const ADKE_FREE_CATEGORY_3_INACTIVE As Integer = 303
        Public Const ADKE_FREE_CATEGORY_4_INACTIVE As Integer = 304
        Public Const ADKE_FREE_CATEGORY_5_INACTIVE As Integer = 305
        Public Const ADKE_FREE_CATEGORY_6_INACTIVE As Integer = 306
        Public Const ADKE_FREE_CATEGORY_7_INACTIVE As Integer = 307
        Public Const ADKE_FREE_CATEGORY_8_INACTIVE As Integer = 308
        Public Const ADKE_FREE_CATEGORY_9_INACTIVE As Integer = 309
        Public Const ADKE_FREE_CATEGORY_10_INACTIVE As Integer = 310

        Public Const ADKE_DBTABLE_ARTICLE_PARCEL As Integer = 311
        Public Const ADKE_DBTABLE_PURCHASE_PRICE As Integer = 312
        Public Const ADKE_DBTABLE_ORDER As Integer = 313
        Public Const ADKE_DBTABLE_ORDER_ROW As Integer = 314
        Public Const ADKE_DBTABLE_OFFER As Integer = 315
        Public Const ADKE_DBTABLE_OFFER_ROW As Integer = 316
        Public Const ADKE_DBTABLE_BOOKING_ROW As Integer = 317
        Public Const ADKE_DBTABLE_FREE_CATEGORIES As Integer = 318

        Public Const ADKE_INVALID_GENDER As Integer = 319
        Public Const ADKE_MEMBER_OF_FAMILY_NOT_OWN_ADRESS As Integer = 320

        Public Const ADKE_SORTORDER_NOT_IN_ADM_VER As Integer = 321
        Public Const ADKE_EDIT_DENIED_MARKED_NOT_DONE As Integer = 322
        Public Const ADKE_INVOICE_EDIT_DENIED_CUSTOMER_NAME As Integer = 323
        Public Const ADKE_ORDER_EDIT_DENIED_ROWS_IN_LEDGER As Integer = 324
        Public Const ADKE_SWIFT_AND_BG_NECESSARY As Integer = 325

        Public Const ADKW_CHANGE_OF_CURRENCY_CODE As Integer = 326
        Public Const ADKW_CHANGE_OF_CURRENCY_RATE As Integer = 327
        Public Const ADKW_CHANGE_OF_CUSTOMER_OTHER_CURRENCY As Integer = 328
        Public Const ADKE_MEMBER_IN_FAMILY As Integer = 329
        Public Const ADKE_DATE2_ONLY_FOR_MEMBER_FEE As Integer = 330
        Public Const ADKE_MEMBER_FEE_MUST_HAVE_DATE2 As Integer = 331

        '//Ink följesedel
        Public Const ADKE_DBTABLE_DELIVERY_NOTE As Integer = 332
        Public Const ADKE_INCOMING_ORDER_EXISTS_SUPPLIER As Integer = 333
        Public Const ADKE_INCOMING_ORDER_EXISTS_ARTICLE As Integer = 334
        Public Const ADKE_DELIVERY_NOTE_ON_SUPPLIER_INVOICE As Integer = 335
        Public Const ADKE_SUPPLIER_NOT_ON_DELIVERY_NOTE As Integer = 336
        Public Const ADKE_DELETE_DENIED_DELIVERY_NOTE_ROWS As Integer = 337
        Public Const ADKE_ACCOUNT_FOR_DEBT_DELIVERY_NOTE_MISSING As Integer = 338
        Public Const ADKE_UPDATE_DENIED_DELETE As Integer = 339
        Public Const ADKE_INVALID_CONNECTION_TYPE As Integer = 340

        '//Leverantörsfaktura
        Public Const ADKE_INVALID_DELNOTEROW As Integer = 341
        Public Const ADKE_DELNOTEROW_NOT_ON_DOCUMENT As Integer = 342
        Public Const ADKE_BOOKROW_NOT_ON_DOCUMENT As Integer = 343
        Public Const ADKE_INVALID_DOCUMENT As Integer = 344
        Public Const ADKE_INVOICENR_ALREADY_EXIST As Integer = 345
        Public Const ADKE_UPDATE_ROW_INCORRECT As Integer = 346
        Public Const ADKE_BOOKROW_NOT_ON_DOC_FIELD_MISSING As Integer = 347

        Public Const ADKE_OPENEX As Integer = 348
        Public Const ADKE_RESET_STRUCT As Integer = 349
        Public Const ADKE_NOT_ALLOWED_RECOPY_COMPANY As Integer = 350

        '//Kollihantering
        Public Const ADKE_DBTABLE_PACKAGE_HEAD As Integer = 351
        Public Const ADKE_DBTABLE_PACKAGE_ROW As Integer = 352
        Public Const ADKE_DBTABLE_IMP_PACKAGE_HEAD As Integer = 353
        Public Const ADKE_DBTABLE_IMP_PACKAGE_ROW As Integer = 354
        Public Const ADKE_DIVIDE_MORE_THAN_DELIVERED As Integer = 355
        Public Const ADKE_DIVIDE_NEGATIVE_VALUE As Integer = 356
        Public Const ADKE_PACKAGE_REF_NOT_VALID As Integer = 357
        Public Const ADKE_PACKAGE_ORDERROW_NOT_VALID As Integer = 358
        Public Const ADKE_PACKAGE_NOT_CREATED As Integer = 359
        Public Const ADKE_INVALID_PACKAGE As Integer = 360
        Public Const ADKE_PACKAGE_CHANGE_NOT_ALLOWED As Integer = 361
        Public Const ADKE_NO_CHANGE_ORDERROW_WITH_PACKAGE As Integer = 362
        Public Const ADKE_CUSTOMER_INVALID_FORPACKTYP As Integer = 363

        Public Const ADKE_WEBSHOP_NOT_ACTIVE As Integer = 364
        Public Const ADKE_EDI_NOT_ACTIVE As Integer = 365
        Public Const ADKE_DELNOTE_NOT_CREATED_BY_EDI As Integer = 366
        Public Const ADKE_PACKAGE_REF_CHANGE_NOT_ALLOWED As Integer = 367
        Public Const ADKE_PACKAGE_NUMBER_NOT_ALLOWED As Integer = 368

        Public Const ADKE_CHANGE_NOT_ALLOWED_DELNOTE_NOT_PRINT As Integer = 369
        Public Const ADKE_CHANGE_NOT_ALLOWED_ORDER_OBLITERATED As Integer = 370
        Public Const ADKE_CHANGE_NOT_ALLOWED_ORDER_PRINTED As Integer = 371
        Public Const ADKE_DOC_SENT_WAITING_RECEIPT As Integer = 372

        Public Const ADKE_LAST_ERROR_CODE As Integer = 372


        Public Enum ADK_SORT_ORDER

            'Gemensamma (Customer, Offer, Order, Invoice)
            eCustomerNr = 0                                     'ADK_CUSTOMER_NUMBER, ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eCustomerName = 1                                   'ADK_CUSTOMER_NAME, ADK_OOI_HEAD_CUSTOMER_NAME - eChar

            'Gemensamma (SupplierInvoice, Supplier)
            eSupplierNr = 2                                     'ADK_SUPPLIER_NUMBER, ADK_SUP_INV_HEAD_SUPPLIER_NUMBER - eChar
            eSupplierName = 3                                   'ADK_SUPPLIER_NAME, ADK_SUP_INV_HEAD_SUPPLIER_NAME - eChar

            'Customer
            eCustomerShortName = 4                              'ADK_CUSTOMER_SHORT_NAME - eChar
            eCustomerPostCode = 5                               'ADK_CUSTOMER_ZIPCODE - eChar
            eCustomerSortOrder = 6                              'ADK_CUSTOMER_SORT_ORDER - eChar

            'Offer
            eOfferNr = 7                                        'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOfferDate = 8                                      'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate

            'Order
            eOrderNr = 9                                        'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOrderDate = 10                                     'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate
            eOrderDeliveryDate = 11                             'ADK_OOI_HEAD_DOCUMENT_DATE2 - eDate

            'Invoice
            eInvoiceNr = 12                                     'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eInvoiceDate = 13                                   'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate

            'SupplierInvoice
            eSupplierInvoiceNr = 14                             'ADK_SUP_INV_HEAD_GIVEN_NUMBER - eDouble
            eSupplierInvoiceExpireDate = 15                     'ADK_SUP_INV_HEAD_DUE_DATE - eDate

            'Article
            eArticleNr = 16                                     'ADK_ARTICLE_NUMBER, ADK_INVENTORY_ARTICLE_ARTICLE_NUMBER - eChar
            eArticleName = 17                                   'ADK_ARTICLE_NAME, ADK_INVENTORY_ARTICLE_ARTICLE_NAME - eChar
            eArticleShortName = 18                              'ADK_ARTICLE_SHORT_NAME - eChar
            eArticleSortOrder = 19                              'ADK_ARTICLE_SORT_ORDER - eChar
            eArticleGroup = 20                                  'ADK_ARTICLE_GROUP, ADK_INVENTORY_ARTICLE_ARTICLE_GROUP - eChar

            'Account
            eAccountNr = 21                                     'ADK_ACCOUNT_NUMBER - eChar
            eAccountText = 22                                   'ADK_ACCOUNT_TEXT - eChar

            'Project
            eProjectCode = 23                                   'ADK_PROJECT_CODE_OF_PROJECT - eChar
            eProjectName = 24                                   'ADK_PROJECT_NAME - eChar

            'Supplier
            eSupplierShortName = 25                             'ADK_SUPPLIER_SHORT_NAME - eChar
            eSupplierSortOrder = 26                             'ADK_SUPPLIER_SORT_ORDER - eChar
            eSupplierTelephone = 27                             'ADK_SUPPLIER_TELEPHONE - eChar

            'Code of Delivery
            eCodeOfTermsOfDelivery = 28                         'ADK_CODE_OF_TERMS_OF_DELIVERY_CODE - eChar

            'Way of Delivery
            eCodeOfWayOfDelivery = 29                           'ADK_CODE_OF_WAY_OF_DELIVERY_CODE - eChar

            'Code of Peyment Terms
            eCodeOfTermsOfPayment = 30                          'ADK_CODE_OF_TERMS_OF_PAYMENT_CODE - eChar

            'Code Of Language
            eCodeOfLanguage = 31                                'ADK_CODE_OF_LANGUAGE_CODE - eChar
            eCodeOfLanguageText = 32                            'ADK_CODE_OF_LANGUAGE_TEXT - eChar

            'Code Of Currency
            eCodeOfCurrency = 33                                'ADK_CODE_OF_CURRENCY_CODE - eChar

            'Code of Customer Category
            eCodeOfCustomerCategory = 34                        'ADK_CODE_OF_CUSTOMER_CATEGORY_CODE - eChar

            'Code of District
            eCodeOfDistrict = 35                                'ADK_CODE_OF_DISTRICT_CODE - eChar

            'Code of Seller
            eCodeOfSeller = 36                                  'ADK_CODE_OF_SELLER_SIGN - eChar

            'Discount agreement
            eDiscountAgreementNr = 37                           'ADK_DISCOUNT_AGREEMENT_CODE - eChar
            eDiscountAgreementText = 38                         'ADK_DISCOUNT_AGREEMENT_TEXT - eChar

            'Code of Article Group
            eCodeOfArticleGroup = 39                            'ADK_CODE_OF_ARTICLE_GROUP_CODE - eChar

            'Code of Article Account
            eCodeOfArticleAccount = 40                          'ADK_CODE_OF_ARTICLE_ACCOUNT_CODE - eChar

            'Code of Unit
            eCodeOfUnit = 41                                    'ADK_CODE_OF_UNIT_CODE - eChar

            'Code of Profit Centre
            eCodeOfProfitCentreUnit = 42                        'ADK_CODE_OF_PROFIT_CENTRE_CODE - eChar

            'Code of PriceList
            eCodeOfPriceList = 43                               'ADK_CODE_OF_PRICE_LIST_CODE - eChar

            'Offer
            eOfferNotOrderCustomerNr = 44                       'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eOfferNotPrintedOfferNr = 45                        'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOfferNotPrintedDate = 46                           'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate
            eOfferNotPrintedCustomerNr = 47                     'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eOfferNotPrintedCustomerName = 48                   'ADK_OOI_HEAD_CUSTOMER_NAME - eChar

            'Order
            eOrderNoInvoiceCustomerNr = 49                      'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eOrderBackorderOrderNr = 50                         'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOrderNotPrintedOrderNr = 51                        'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOrderNotPrintedDate = 52                           'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate
            eOrderNotPrintedCustomerNr = 53                     'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eOrderNotPrintedCustomerName = 54                   'ADK_OOI_HEAD_CUSTOMER_NAME - eChar
            eOrderNotPrintedDeliveryDate = 55                   'ADK_OOI_HEAD_DOCUMENT_DATE2 - eDate
            eOrderNoDeliveryNoteOrderNr = 56                    'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eOrderNoDeliveryNoteDate = 57                       'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate
            eOrderNoDeliveryNoteCustomerNr = 58                 'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eOrderNoDeliveryNoteCustomerName = 59               'ADK_OOI_HEAD_CUSTOMER_NAME - eChar
            eOrderNoDeliveryNoteDeliveryDate = 60               'ADK_OOI_HEAD_DOCUMENT_DATE2 - eDate

            'Invoice
            eInvoiceInledgerExpireDate = 61                     'ADK_OOI_HEAD_DOCUMENT_DATE2 - eDate
            eInvoiceInledgerCustomerNr = 62                     'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eInvoiceInledgerCustomerName = 63                   'ADK_OOI_HEAD_CUSTOMER_NAME - eChar
            eInvoiceInledgerInvoiceNr = 64                      'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eInvoiceNotPayedCustomerNr = 65                     'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eInvoiceNotPayedDate = 66                           'ADK_OOI_HEAD_DOCUMENT_DATE2 - eDate
            eInvoicePayedCustomerNr = 67                        'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eInvoiceManuellInvoiceNr = 68                       'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eInvoiceNotPrintedInvoiceNr = 69                    'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eInvoiceNotPrintedDate = 70                         'ADK_OOI_HEAD_DOCUMENT_DATE1 - eDate
            eInvoiceNotPrintedCustomerNr = 71                   'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eInvoiceNotPrintedCustomerName = 72                 'ADK_OOI_HEAD_CUSTOMER_NAME - eChar
            eInvoiceNoIntrestInvoiceNr = 73                     'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble
            eInvoiceNoIntrestCustomerNr = 74                    'ADK_OOI_HEAD_CUSTOMER_NUMBER - eChar
            eInvoiceAgreementInvoiceNr = 75                     'ADK_OOI_HEAD_DOCUMENT_NUMBER - eDouble

            'SupplierInvoice
            eSupplierInvoiceNotPayedInvoiceNr = 76              'ADK_SUP_INV_HEAD_GIVEN_NUMBER - eDouble
            eSupplierInvoiceNotPayedSupplierNr = 77             'ADK_SUP_INV_HEAD_SUPPLIER_NUMBER - eChar
            eSupplierInvoiceNotPayedSupplierName = 78           'ADK_SUP_INV_HEAD_SUPPLIER_NAME - eChar
            eSupplierInvoiceNotPayedExpireDate = 79             'ADK_SUP_INV_HEAD_DUE_DATE - eDate
            eSupplierInvoicePayedSupplierNr = 80                'ADK_SUP_INV_HEAD_SUPPLIER_NUMBER - eChar
            eSupplierInvoicePaymentAbroadInvoiceNr = 81         'ADK_SUP_INV_HEAD_GIVEN_NUMBER - eDouble
            eSupplierInvoicePaymentAbroadSupplierNr = 82        'ADK_SUP_INV_HEAD_SUPPLIER_NUMBER - eChar
            eSupplierInvoicePaymentAbroadSupplierName = 83      'ADK_SUP_INV_HEAD_SUPPLIER_NAME - eChar
            eSupplierInvoicePaymentAbroadExpireDate = 84        'ADK_SUP_INV_HEAD_DUE_DATE - eDate
            eSupplierInvoiceDomesticInvoiceNr = 85              'ADK_SUP_INV_HEAD_GIVEN_NUMBER - eDouble
            eSupplierInvoiceDomesticSupplierNr = 86             'ADK_SUP_INV_HEAD_SUPPLIER_NUMBER - eChar
            eSupplierInvoiceDomesticSupplierName = 87           'ADK_SUP_INV_HEAD_SUPPLIER_NAME - eChar
            eSupplierInvoiceDomesticExpireDate = 88             'ADK_SUP_INV_HEAD_DUE_DATE - eDate
            eSupplierInvoiceNotJournalInvoiceNr = 89            'ADK_SUP_INV_HEAD_GIVEN_NUMBER - eDouble

            'Article
            eArticleLackOf = 90                                 'ADK_ARTICLE_NUMBER - eChar
            eArticlePlaceInStock = 91                           'ADK_ARTICLE_PLACE_IN_STOCK, ADK_INVENTORY_ARTICLE_ARTICLE_PLACE_IN_STOCK - eChar

            'Account
            eAccountActiveNr = 92                               'ADK_ACCOUNT_NUMBER - eChar
            eAccountActiveText = 93                             'ADK_ACCOUNT_TEXT - eChar

            'Project
            eProjectNotFinishedCode = 94                        'ADK_PROJECT_CODE_OF_PROJECT - eChar
            eProjectNotFinishedName = 95                        'ADK_PROJECT_NAME - eChar
            eProjectFinishedCode = 96                           'ADK_PROJECT_CODE_OF_PROJECT - eChar
            eProjectFinishedName = 97                           'ADK_PROJECT_NAME - eChar

            'ManualDeliveryIn
            eManualDeliveryIn = 98                              'ADK_MANUAL_DELIVERY_IN_DOCUMENT_NUMBER - eDouble

            'ManualDeliveryOut
            eManualDeliveryOut = 99                             'ADK_MANUAL_DELIVERY_OUT_DOCUMENT_NUMBER - eDouble

            'Dispatcher
            eDispatcher = 100                                   'ADK_DISPATCHER_ID - eChar

            '//Booking
            eBookingNr = 101   '//ADK_BOOKING_DOCUMENT_NUMBER - eDouble
            eBookingSupplierNumber = 102   '//ADK_BOOKING_SUPPLIER_NUMBER - eChar
            eBookingSupplierName = 103   '//ADK_BOOKING_SUPPLIER_NAME - eChar
            eBookingNotPrintedBookingNr = 104   '//ADK_BOOKING_DOCUMENT_NUMBER - eDouble
            eBookingNotPrintedSupplierNumber = 105   '//ADK_BOOKING_SUPPLIER_NUMBER - eChar
            eBookingNotPrintedDeliveryDate = 106   '//ADK_BOOKING_DELIVERY_DATE - eDate
            eBookingNotDeliveredBookingNr = 107   '//ADK_BOOKING_DOCUMENT_NUMBER - eDouble
            eBookingNotDeliveredSupplierNumber = 108   '//ADK_BOOKING_SUPPLIER_NUMBER - eChar
            eBookingNotDeliveredSupplierName = 109   '//ADK_BOOKING_SUPPLIER_NAME - eChar
            eBookingNotDeliveredSupplierDeliveryDate = 110   '//ADK_BOOKING_DELIVERY_DATE - eDate

            '//CustomerDiscountRow
            eCustomerDiscountRowArtGrp = 111   '//ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_CODE - eChar	
            eCustomerDiscountRowArtNr = 112   '//ADK_CODE_OF_CUSTOMER_DISCOUNT_ROW_CODE - eChar

            '//ArticleParcel
            eArticleParcelRowNrParcel = 113   '//ADK_CODE_OF_ARTICLE_PARCEL_NR - eChar
            eArticleParcelArticleNrParcel = 114   '//ADK_CODE_OF_ARTICLE_PARCEL_NR - eChar
            eArticleParcelArticleNr = 115   '//ADK_CODE_OF_ARTICLE_PARCEL_ARTICLE_NR - eChar

            '//ArticleName
            eArticleNameNr = 116   '//ADK_CODE_OF_ARTICLE_NAME_ARTICLE_NUMBER - eChar

            '//Price
            ePriceArticleNr = 117 '//ADK_PRICE_ARTICLE_NUMBER - eChar	
            ePriceBaseArticleNr = 118 '//ADK_PRICE_ARTICLE_NUMBER - eChar
            ePriceNotBaseArticleNr = 119 '//ADK_PRICE_ARTICLE_NUMBER - eChar	
            ePriceList = 120 '//ADK_PRICE_PRICE_LIST - eChar		
            ePriceBaseList = 121 '//ADK_PRICE_PRICE_LIST - eChar	
            ePriceNotBaseList = 122 '//ADK_PRICE_PRICE_LIST - eChar	

            '//PurchasePrice
            ePurchasePriceArticleNrBase = 123 '//ADK_ARTICLE_PURCHASE_PRICE_ARTICLE_NUMBER - eChar
            ePurchasePriceArticleNrStf = 124 '//ADK_ARTICLE_PURCHASE_PRICE_ARTICLE_NUMBER - eChar
            ePurchasePriceArticleNrAll = 125 '//ADK_ARTICLE_PURCHASE_PRICE_ARTICLE_NUMBER - eChar
            ePurchasePriceSupplierNrBase = 126 '//ADK_ARTICLE_PURCHASE_PRICE_SUPPLIER_NUMBER - eChar
            ePurchasePriceSupplierNrStf = 127 '//ADK_ARTICLE_PURCHASE_PRICE_SUPPLIER_NUMBER - eChar
            ePurchasePriceSupplierArticleNr = 128 '//ADK_ARTICLE_PURCHASE_PRICE_SUPPLIER_NUMBER - eChar

            '//WayOfPayment
            eWayOfPayment = 129 '//ADK_CODE_OF_WAY_OF_PAYMENT_CODE - eChar

            '//Member
            eMemberNr = 130  '//ADK_MEMBER_NUMBER - eChar
            eMemberFirstName = 131   '//ADK_MEMBER_FIRST_NAME - eChar
            eMemberLastName = 132   '//ADK_MEMBER_LAST_NAME - eChar
            eMemberPostCode = 133  '//ADK_MEMBER_ZIPCODE - eChar
            eMemberPersonalNumber = 134  '//ADK_MEMBER_PERSONAL_NUMBER - eChar
            eMemberCategory = 135  '//ADK_MEMBER_CATEGORY - eChar
            eMemberFreeCategory1 = 136  '//ADK_MEMBER_FREE_CATEGORY1 - eChar
            eMemberFreeCategory2 = 137  '//ADK_MEMBER_FREE_CATEGORY2 - eChar
            eMemberFreeCategory3 = 138  '//ADK_MEMBER_FREE_CATEGORY3 - eChar
            eMemberFreeCategory4 = 139  '//ADK_MEMBER_FREE_CATEGORY4 - eChar
            eMemberFreeCategory5 = 140  '//ADK_MEMBER_FREE_CATEGORY5 - eChar
            eMemberFreeCategory6 = 141  '//ADK_MEMBER_FREE_CATEGORY6 - eChar
            eMemberFreeCategory7 = 142  '//ADK_MEMBER_FREE_CATEGORY7 - eChar
            eMemberFreeCategory8 = 143  '//ADK_MEMBER_FREE_CATEGORY8 - eChar
            eMemberFreeCategory9 = 144  '//ADK_MEMBER_FREE_CATEGORY9 - eChar
            eMemberFreeCategory10 = 145  '//ADK_MEMBER_FREE_CATEGORY10 - eChar

            '//DeliveryNote
            eDeliveryNoteNr = 146   '//ADK_DELIVERY_NOTE_DOCUMENT_NUMBER - eDouble	
            eDeliveryNoteSupplierNumber = 147   '//ADK_DELIVERY_NOTE_SUPPLIER_NUMBER - eChar
            eDeliveryNoteSupplierName = 148   '//ADK_DELIVERY_NOTE_SUPPLIER_NAME - eChar
            eDeliveryNoteInvoiceNotSentDeliveryNoteNr = 149   '//ADK_DELIVERY_NOTE_DOCUMENT_NUMBER - eDouble	
            eDeliveryNoteInvoiceNotSentSupplierNumber = 150   '//ADK_DELIVERY_NOTE_SUPPLIER_NUMBER - eChar
            eDeliveryNoteInvoiceNotSentSupplierName = 151   '//ADK_DELIVERY_NOTE_SUPPLIER_NAME - eChar

            '//Package
            ePackageOrderNr = 152

            '//Package Import
            eImpPackageDelNoteNr = 153


        End Enum

        'Streckkodstyper
        Public Enum ADK_BAR_CODE_TYPE

            eEmpty = 0                              'Tom
            eEAN_8 = 1                              'EAN 8
            eEAN_13 = 2                             'EAN 13
            eUPC_A = 3                              'UPC-A
            eUPC_E = 4                              'UPC-E
            eCode_39 = 5                            'Code 39
            eInterleaved_2_5 = 6                    'Interleaved 2/5
            eUS_Postnet = 7                         'US Postnet
            eCode_128 = 8                           'Code 128

        End Enum
        '//Avgiftskod
        Public Enum ADK_CODE_BG_FOREIGN_FEE
            '// Bank                         FB  HB  Nord    SE  Spar
            eBgSenderPays = 0    '// sender pays all              *   *   *       *   *
            eBgRecipPays = 1    '// recipiant pays all         	    *           *
            eBgRecipPaysExpr = 2    '// recipiant pays express fee                   *
            eBgRecipPaysForeign = 3     '// recipiant pays foreign fee   *   *   *       *   *

        End Enum
        'Vad ska kopieras vid Kreditfaktura?
        Public Enum ADK_CREDIT_INVOICE_TO_COPY
            eEntireInvoice = 0                      'The entire Invoice
            eInformation = 1                        'Customer/Supplier-Information
            eNothing = 2                            'Nothing
        End Enum

        Public Enum ADK_CODE_OF_SEX

            eSexMale = 0    '//Man
            eSexFemale = 1    '//Kvinna
            eSexNone = 2    '//Saknas = juridisk person

        End Enum



        Public Enum ADK_ERROR_TEXT_TYPE
            elRc = 0
            elDbTable = 1
            elField = 2
            elFunction = 3
            elProgramPart = 4

        End Enum

        Public Structure ADKERROR

            Dim lRc As Integer
            Dim lDbTable As Integer
            Dim lField As Integer
            Dim lFunction As Integer
            Dim lProgramPart As Integer

        End Structure

        Public Enum ADK_FIELD_TYPE
            eUnused = 0
            eBool = 1
            eChar = 2
            eDouble = 4
            eDate = 5
            eData = 6
        End Enum


        Public Declare Function AdkCreateData Lib "adk.dll" _
        Alias "_AdkCreateData@4" (ByVal IDataBaseId As Integer) As Integer
        Public Declare Function AdkCreateDataRow Lib "adk.dll" _
        Alias "_AdkCreateDataRow@8" (ByVal IDataBaseId As Integer, ByVal iNumber As Integer) As Integer
        Public Declare Function AdkGetDataRow Lib "adk.dll" _
        Alias "_AdkGetDataRow@8" (ByVal pDb As Integer, ByVal iIndex As Integer) As Integer
        Public Declare Function AdkDeleteStruct Lib "adk.dll" _
        Alias "_AdkDeleteStruct@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkOpen Lib "adk.dll" _
        Alias "_AdkOpen@8" (ByVal pszSystemPath As String, _
        ByVal pszFtgPath As String) As ADKERROR
        Public Declare Function AdkClose Lib "adk.dll" _
        Alias "_AdkClose@0" () As Integer

        Public Declare Sub AdkGetErrorText Lib "adk.dll" Alias "_AdkGetErrorText@16" _
        (ByRef Error1 As ADKERROR, ByVal nErrorTextType As Integer, ByRef achBuf As String, _
        ByVal Ilen As Integer)

        Public Declare Function AdkGetStr Lib "adk.dll" Alias "_AdkGetStr@16" _
        (ByVal pDb As Integer, ByVal iFieldId As Integer, ByRef ppsValue As String, ByVal Ilen _
        As Integer) As ADKERROR
        Public Declare Function AdkGetBool Lib "adk.dll" Alias "_AdkGetBool@12" (ByVal pDb As Integer _
        , ByVal iFieldId As Integer, ByRef pbValue As Integer) As ADKERROR
        Public Declare Function AdkGetData Lib "adk.dll" Alias "_AdkGetData@16" (ByVal pDb As Integer _
        , ByVal iFieldId As Integer, ByVal iIndex As Integer, ByRef pDbRow As Integer) As ADKERROR
        Public Declare Function AdkGetDouble Lib "adk.dll" Alias "_AdkGetDouble@12" (ByVal pDb As Integer _
        , ByVal iFieldId As Integer, ByRef pdValue As Double) As ADKERROR
        Public Declare Function AdkGetDate Lib "adk.dll" Alias "_AdkGetDate@12" (ByVal pDb As Integer _
        , ByVal iFieldId As Integer, ByRef pdaValue As Integer) As ADKERROR

        Public Declare Function AdkGetDecimals Lib "adk.dll" Alias "_AdkGetDecimals@12" _
        (ByVal pDb As Integer, ByVal iFieldId As Integer, ByRef iDec As Integer) As ADKERROR
        Public Declare Function AdkIsReadWrite Lib "adk.dll" Alias "_AdkIsReadWrite@8" _
        (ByVal pDb As Integer, ByVal iFieldId As Integer) As Boolean

        Public Declare Function AdkSetStr Lib "adk.dll" Alias "_AdkSetStr@12" (ByVal pDb As Integer, _
        ByVal iFieldId As Integer, ByVal pValue As String) As ADKERROR
        Public Declare Function AdkSetBool Lib "adk.dll" Alias "_AdkSetBool@12" (ByVal pDb As Integer, _
        ByVal iFieldId As Integer, ByVal bValue As Integer) As ADKERROR
        Public Declare Function AdkSetData Lib "adk.dll" Alias "_AdkSetData@12" (ByVal pDb As Integer, _
        ByVal iFieldId As Integer, ByVal pDataRow As Integer) As ADKERROR
        Public Declare Function AdkSetDouble Lib "adk.dll" Alias "_AdkSetDouble@16" (ByVal pDb As Integer, _
        ByVal iFieldId As Integer, ByVal dValue As Double) As ADKERROR
        Public Declare Function AdkSetDate Lib "adk.dll" Alias "_AdkSetDate@12" (ByVal pDb As Integer, _
        ByVal iFieldId As Integer, ByVal lValue As Integer) As ADKERROR

    Public Declare Function AdkIntegerToDate Lib "adk.dll" Alias "_AdkLongToDate@12" (ByVal lValue As Integer, _
        ByVal ppsValue As String, ByVal Ilen As Integer) As ADKERROR
    Public Declare Function AdkDateToLong Lib "adk.dll" Alias "_AdkDateToLong@8" (ByVal sValue As String, _
        ByRef plValue As Integer) As ADKERROR

        Public Declare Function AdkGetType Lib "adk.dll" Alias "_AdkGetType@12" (ByVal pDb As Integer, ByVal _
        iFieldId As Integer, ByRef pType As ADK_FIELD_TYPE) As ADKERROR
        Public Declare Function AdkGetLength Lib "adk.dll" Alias "_AdkGetLength@12" (ByVal pDb As Integer, ByVal _
        iFieldId As Integer, ByVal iLength As Integer) As ADKERROR

        Public Declare Function AdkAdd Lib "adk.dll" Alias "_AdkAdd@4" (ByVal pDb As Integer) As ADKERROR
        Public Declare Function AdkUpdate Lib "adk.dll" Alias "_AdkUpdate@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkSetSortOrder Lib "adk.dll" Alias "_AdkSetSortOrder@8" (ByVal pDb As Integer, ByVal nSortOrder As ADK_SORT_ORDER) As ADKERROR
        Public Declare Function AdkNext Lib "adk.dll" _
         Alias "_AdkNext@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkPrevious Lib "adk.dll" _
         Alias "_AdkPrevious@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkFirst Lib "adk.dll" _
        Alias "_AdkFirst@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkLast Lib "adk.dll" _
        Alias "_AdkLast@4" (ByVal pDb As Integer) As ADKERROR

        Public Declare Function AdkFind Lib "adk.dll" Alias "_AdkFind@4" (ByVal pDb As Integer) As ADKERROR
        'Public Declare Function AdkFind Lib "adk.dll" Alias "_AdkFind@4" (ByRef pDb As Integer) As ADKERROR

        Public Declare Function AdkGetFieldName Lib "adk.dll" Alias "_AdkGetFieldName@16" (ByVal IDataBaseId As _
        Integer, ByVal iFieldId As Integer, ByRef ppsValue As String, ByVal Ilen As Integer) As ADKERROR

        Public Declare Function AdkDelete Lib "adk.dll" Alias "_AdkDelete@4" (ByVal pDb As Integer) As ADKERROR
        Public Declare Function AdkResetStruct Lib "adk.dll" Alias "_AdkDelete@4" (ByVal pDb As Integer) As ADKERROR
        Public Declare Function AdkDeleteRow Lib "adk.dll" Alias "_AdkDeleteRow@8" (ByVal pDb As Integer, ByVal iIx As Integer) As ADKERROR

        Public Declare Sub AdkSetWarningFunction Lib "adk.dll" Alias "_AdkSetWarningFunction@4" (ByVal pFunction As Integer)

        Public Declare Function GetPrivateProfileString Lib "kernel32" Alias _
        "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
        ByVal lpKeyName As String, ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Integer, ByVal lpFileName As String) As Integer

        Public Declare Function GetWindowsDirectory Lib "kernel32" Alias _
        "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Integer) As Integer


        Public Sub CallBackWarningFunction(ByRef Error2 As ADKERROR)
            MsgBox("Varning!!")
        End Sub






End Module
